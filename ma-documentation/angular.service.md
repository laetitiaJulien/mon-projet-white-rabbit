# Angular Service

## Comment lier Angular avec node

Node est une API de CRUD (lire, écrire, modifier des choses) et a des applications externe de leurs parler.
Donc on va utiliser Angular pour lui poser des questions. 

 **attention** ne pas faire les appels des fonctions dans le compenent de Typescrip. Il faut pour cela utiliser le service
 
 
 ### Le service
 
Comme les compenent et les modules c'est une classe qui a la particularité d'utiliser le singleton.
 
**singleton** est une classe qui a qu'une seul instance et qui n'éxiste que dans le cadre de leurs module  
principe universel de developpement (livre design patterns)

#### Créer un service 

- Aller dans le terminal et taper ceci (hotels car il nous aide à trouver des hotels)

![ng service](assets/ngservice.PNG)

- Effacer le fichier spec.ts généré car il on ne va s'en sert pas dans le cas présent 
et gitter l'autre


## Faire des appels au backoffice

Pour faire les appels au backOffice on va avoir besoin de faire des appels http. 
En Angular pour se servir d'un outil il faut le déclarer dans le constructor. 
Mais pour faire les appels on a également besoin d'appeler les HttpClientModule dans le app.module 

![HttpClient](assets/HttpClientModule.PNG)

Pour ce servire de cette outil, il faut le notifier dans le constructeur 
mettre le mot clé Private pour signifier qu'on l'utilise que dans le constructor. 
et donner un nom à l'appel ( pour ne pas s'embêter à donner un nom shift + f6 et l'auto-complétion se fait automatiquement)

![nom HttpClient](assets/nomhttpClient.PNG)


Ensuite on veut faire un GET avec donc une méthode (ou function mais qui dans une class) on a plus besoin 
du mot function. 
Et dans get on met notre url et le get renvoie un observable 

![get de service](assets/getservice.PNG)


**observable** comme les promises dans node mais plus complexe à manipuler. On peut comparer ça un grand tuyau où l'on
passe des data, on va écouter les data et où on s'abonner aux data (subscribe)

dans un observable on utilise pas de .then mais pipe. 

**pipe** sert à traiter la data obtenue. 

![pipe](assets/pipe.PNG)

Pour préciser ce que va nous renvoyer get comme type de data on peut le mettre entre <> 

![typeget](assets/typeget.PNG)

Ensuite dans le pipe on peut utiliser des opérateur de manipulation. 
(mettre des tuyaus annexe)

- Premier opérateur tap pour faire le console.log de hostels

![tap](assets/tap.PNG)


**subscribe** ne se fait pas dans le service 
 


 ²





