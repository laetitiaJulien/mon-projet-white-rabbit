# MARK DOWN

## Qu'est ce que le Mark Down ?



Le Mark down est une façon plus simple d'écrire du HTML
c'est un language de prise de note une sorte de word simplifié.
 
Avant de ce lancer dans l'écriture a proprement parlé, il faut créer **une structure**.
Un plan de notre projet.
Créer des parties ou des sous parties.

L'architecture est importante notamment pour le SIO (référencement).

***ATTENTION*** : HTML et CSS ne sont pas des languages de programmation mais des languages de markdown.



## Comment utiliser le  Markdown ?

```
# = titre 1(h1)

## = titre 2 (h2) 

*objet* = italique (

**objet** = Gras(strong)

*objet 1
*objet 2
*objet 3 = liste (li)

1 ...
2 ...
3 objets = liste chiffrés (ol)

≥ = bloquote 
≥≥ = bloquote dans un bloquote

Backticks = 3 (`) pour créer un block de code


```
***ATTENTION*** Les typologies de MarkDown ne sont pas toujours les mêmes 

*ex: ##Titre si je ne met pas l'espace rien ne se passe.*



## Qu'est ce que GIT ?

GIT est un protocole qui permet de stocker du contenu. 
HUB et LAB UTILISE GIT 

## Utiliser Markdown dans GITLAB ?

### Créer un New Projet 

1.Aller dans GITLAB

2.Cliquer dans l'onglet Projet 

3.A droite cliquer sur New Projet 

4.Renseigner le nom du projet 

5.Renseigner la description du projet 

6.Mettre en publique 

7.Cocher Initialize repository with a README

8.Create project

### Ouvrir le projet dans Webstorm 

1.Copier l'URL http en haut à gauche dans clone 

2.Ouvrir Webstorm et cliquer sur check out 

3.Choisir GIT 

4.Copier le lien.


### Sauvegarde du projet

1.Dans Webstorm cliquer sur la flèche verte en haut à droite

3.Décocher ce qui ce trouve sur la droite.

4.Expliquer le projet de manière brève dans Commit Message

5.Cliquer sur commit 

6.Une fois le doc enregistré cliquer dans le menu sur VCS-≥ GIT -> Push puis Envoyer  


## Vocabulaire vue dans ce cours  

*Boilerplate* = projet ou ensemble prêt à être mis en place

*Bootstrape* =  Framework comme CSS pour les nouveaux projet utilisé par exemple TWITTER  
``
*from scratch* = recommencer à zéro un projet 

*WISIWIG* = What i see what i get 
permet de récupérer dans sa forme initial un projet et qui permet d'éditer du contenu dans un navigateur.














 