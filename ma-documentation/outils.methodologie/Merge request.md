# La merge request 

## Les commits 

Les commits sont important et indispensable pour toujours avoir une trace du code créé.
Cela prouve également le professionnalisme du développeur.

## Pourquoi utiliser une méthode de travail en équipe

Quand l'équipe travail sur un projet commun, elle se doit d'utiliser une méthode de travail spécifique car toute intervention 
sur un fichier créé une modification du code.
Si chacun créé sa propre  version du fichier initial, il est difficile de tout rassembler à la fin de la journée.

### La méthode Gitflow

La méthode Gitflow ce composent d'une première version qui s'appelle 
la MASTER.  

#### La master 

La master est la version officielle d'un projet. C'est la version qui va être déployé sur un CDN.
Elle ne doit **jamais être modifier**  

Chaque copie de ce premier fichier s'appelle une branche.

#### Les branches 

Les branches son finalement les versions que vont utiliser les développeurs pour créer leurs parties du code.
Elles alimenteront la MASTER quand ils auront terminés, en liant les deux parties (branche, master).  
C'est ce qu'on appel une MERGE REQUEST.

##### Comment créer une branche

- Sur webstorm aller dans Git:master, en bas à droite 

![capture d.ecran](../assets/Capture.webstorm.branche.png)

![capture d.ecran2](../assets/capture.webstorm.VCS.branche.png)

- Pour donner un nom à la branche commencer par feature ensuite mettre '/' l'objet du travail écrit en camel case

![capture d.ecran3](../assets/donnernombranche.png)

3. Démarer le travail en question et commit

##### Comment changer de branche

Pour changer de branche il faut revenir sur Git:feature ou master et puis cliquer sur checkout



#### Valider avec des Merges 


- Pour Merger il faut retourner sur la Master est faire un checkout

- Ensuite pour aller dans VCS; cliquer sur GIT puis merge changes

![capture.merge](../assets/merger.png)

- il y a le choix entre
    * feature change ..., (stocker sur l'ordi)
    * remote origin feature change (stocker sur gitlab)
    

- cliquer sur feature change header puis merger

- Faire un commit puis push

- Penser à delete la branche 


#### Valider avec des Merges request

Le Merge request permet de faire valider sont travail auprès d'une tierce personne.
Notamment son manager.

1. Penser à faire Commit/Push 

2. Aller dans Gitlab et ce mettre sur son projet 

3. Cliquez sur Merge request à gauche

4. Cliquez sur demande

5. Cliquez sur create merge request

6. Change branches


#### Les conflits 

Lorsque deux personnes modifie la même partie du code, webstorm détecte un conflit. 
Pour résoudre le conflit webstorm affiche à l'écran 2 parties detectées comme conflictuelle et 
une autre fenêtre avec la version master.
Le responsable n'a plus qu'a décider la version qu'il veut sauvegarder.
Une seule version ne peut être validée.
Ensuite penser à pusher
La plus part du temps ce n'est pas au responsable de résoudre les conflits mais plutôt au membre de l'équipe de ce
mettre d'accord



 


