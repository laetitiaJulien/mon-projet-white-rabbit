# Hébergement 

## Comment ce compose un site ?

Un site ce compose de deux parties:

* Une partie dynamique (BD, DATA)

* Une partie statique (Html, Javascript, CSS) 

![shémas](../ma-documentation/assests/Capture%20d’écran%202019-09-30%20à%2021.28.37.png)

La partie dynamique comunique avec la partie statique.

## Qu'est ce qu'une URL 

Une Url c'est comme une adresse postal, elle permet de donner à un client les informations d'un site (Html,css...)
Le point d'entrée d'un site est toujours un fichier Html que l'ont nomme Index.html.
Une url est placé sur un serveur.

## Qu'est ce qu'un CDN 

La manière la plus simple de communiquer un contenu est de ne pas utiliser un seul server mais plusieurs, pour éviter la surcharge.
Cette ensemble de servers, on appel ça un CDN (Content Delivery Network) 



## Qu'est ce que FIREBASE

Firebase est un outil qui permet d'utiliser *Hosting* qui est un CDN.

### Comment mettre sont site en ligne avec Firebase Hosting

1. Créer un compte Firebase

2. Depuis la console créer un projet 

3. Une fois le projet créer, allez dans hosting

4. Cliquer sur le bouton commencer

5. Copier le lien

#### Partie version contrôle 

1. Une fois dans webstorm ouvrir *version contrôle*

2. Coller le lien dans *version contrôle* puis attendre le temps que tous s'intalle

3. Ce login sur Hosting puis taper init login dans Webstorm

4. Répondre OUi à ready to go

5. choisir le projet 

6. Mettre dans la console login init hosting (faire attention d'être dans le bon dossier)

7. Use a project 

8. Créer un dossier *public*

9. Déplacer les dossiers à l'intérieur (cut/past)

10. Copier le dossier *home* à la racine du projet (dossier public)

11. Répondre oui à la question le dossier est-il public

12. Répondre non à la question faite vous une single page

13. Répondre non à la question est ce que vous voulez écraser le fichier

14. Taper firebase deploy 

15. Mettre dans le .gitignore le dossier .firebase

16. Faire GIT ADD sur chacun des fichiers 


**Vocabulaire**

*Déploiement* = passer une maquette sur internet 

*Point d'entrée* = permet à Firebase de faire le déploiemeent. 
Ce point d'entré est un fichier index.html 

*Single page* = est une page fait avec ANGULAR ou REACT

*CDN* = ensemble de server à travers le monde copiant tous le même contenu ou plutôt les mêmes ressources. 
facilite la distribution du site à travers le monde.


 

 





