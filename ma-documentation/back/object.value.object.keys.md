# Object values et object keys

## Object 

>Un objet est un ensemble de données qui compose une même caractéristique

exemple: si je vend des voitures; le mot voiture seras composé de plusieurs caractéristiques:

- la couleur 
- la marque
- le prix...

## Object Values

la valeur d'un objet sont ses caractéristiques.

exemple: 

- la couleur = *bleu*
- la marque = *nissan*
- le prix = *15000 euros* 

##  object Keys 


Les clefs sont le nom de ces valeurs 

donc la couleur que l'ont peut appeler
 - color
 - brand
 - price

## Reprendre toutes les clefs dans un tableau 

Pour reprendre toutes les clefs dans un tableau il faut faire un console.log 
de l'objet.

``````
const my {

color : blue 
brand : nissan 
price : 15000

}

console.log(object.keys(my))

```````

Maintenant on aimerais modifier l'ensemble de ces clefs et les mettre en majuscule:
Pour cela on va utiliser un .map

``````

const my {

color : blue 
brand : nissan 
price : 15000

}

const up = object.keys(my).map(keys => keys.toUppercase());
console.log(up)

``````

Pour récupérer  à la fois les valeurs et les clefs on va parcourir l'objets et stocker les valeurs dans 
les clefs. 



et si on veut ajouter 10 on rajoute + 10 

![capturekeys+10](../assets/objects10.png)


## Récuperer des valeurs 

Si on veut par exemple prendre des valeurs qui seront du coup 
des stings et les mettre coller les uns  aux autres, on va devoir reprendre ces valeurs et faire un reduce 
pour les assembler


![capture de object.value](../assets/reduce.object.value.png)


