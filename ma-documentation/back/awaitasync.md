# Await et Async 

## Qu'est que c'est ?

>Cela permet de remplacer les promesses utilisées avant. C'est une manière différente de gérer les promesses.

Avec Await et Async on utilise toujours try et catch 

## Application 

Exemple on veut récupérer dans la base de données un des hotels et on veut qu'il y ai un tag isvalidated. 

### Ancienne version ça donne ceci :
Avec beaucoup de *.then* imbriquer 

![ancienne version](../assets/oldpromise.PNG)

***as*** est un caster permettant de changer le type de quelque chose 

Ne pas oublier de modifier hotelModel et de lui ajouter isvalidated en précisant que c'est un booléen

![isvalidates](../assets/isvalidated.PNG)

### Nouvelle version avec les await et les async 

La première chose est de déclarer async dans la fonction

````javascript
app.get('/', async function (req, res) {
  
    try {
        
        
        
        
    }
    
     catch{
        
        
        
        
        
     }
    
})

````

La deuxième chose est de créer une const hotel = await 

>**await** signifie attendre d'avoir le resultat avant de l'executer, et en attendant on le place dans la const 
hotelSnapshot 

````javascript

app.get('/', async function (req, res) {
  
    try {
       const hotelSnapshot = await db
        .collection('hostel')
        .doc('fgssdrgzeth')// id 
        .get()   
       
        
    }
    
     catch (e){
        
        
        
        
        
     }
    
})
````

. Ensuite créer une autre const pour voir la data.

. Ne pas oublier de tous typer. 

. On met le booléen puis on envoi avec un res.send 

On peut mettre un console.log de hotel 

````javascript

app.get('/', async function (req, res) {
  
    try {
       const hotelSnapshot = await db
        .collection('hostel')
        .doc('fgssdrgzeth')// id 
        .get();   
      const hotel: HotelModel = hotelSnapshot.data() as HotelModel; // remplace .then  mettre le type
       hotel.isValidated = true;
        console.log(hotel)
        res.send(hotel)
    }
    
     catch (e){
        
        
        
        
        
     }
    
})
````

Aprés on gère le catch 

````javascript

app.get('/', async function (req, res) {
  
    try {
       const hotelSnapshot = await db
        .collection('hostel')
        .doc('fgssdrgzeth')// id 
        .get();   
      const hotel: HotelModel = hotelSnapshot.data() as HotelModel; // remplace .then  mettre le type
       hotel.isValidated = true;
        console.log(hotel)
        res.send(hotel)
    }
    
     catch (e){
        
        console.log(e)
    }
    
})
````

Maintenant on veut appeler un deuxième hotel

````javascript

app.get('/', async function (req, res) {
  
    try {
       const hotelSnapshot = await db
        .collection('hostels')
        .doc('fgssdrgzeth')// id 
        .get();  
      const hotel: HotelModel = hotelSnapshot.data() as HotelModel; // remplace .then  mettre le type 
       
       const hotelSnapshot2 = await db
        .collection('hostels')
        .doc('lighglyl')// id 
        .get();   
      const hotel2: HotelModel = hotelSnapshot.data() as HotelModel; // remplace .then  mettre le type
      
       hotel.isValidated = true;
       hotel2.isValidated = true;
        console.log(hotel, hotel2)
        res.send([hotel, hotel2])
    }
    
     catch (e){
        
        console.log(e)
    }
    
})
````

Seulement dans cette exemple on appel d'abord l'hotel 1 puis l'hotel 2 pour ensuite avoir le résultat.
Mais on voudrait appeler les hotels en même temps. 

Pour cela on va enlever le **await** car quand il n'y a pas de await la promesse se fabrique mais ne s'execute pas. 
Si il y a **async** il faut **await** pour qu'il s'execute 

````javascript

app.get('/', async function (req, res) {
  
    try {
       const hotelPromise = db// on prepare la promesse
        .collection('hostels')
        .doc('fgssdrgzeth')// id 
        .get();  
      
       
       const hotelPromise2 = db
        .collection('hostels')
        .doc('lighglyl')// id 
        .get();   
       
      const [hotelSnapshot, hotel2Snapshot]/* on destruscture pour mieux les utilisées*/ = await Promise.all([ 
      // donc on créer une cont pour stocker les 2 hotel et on 
      //met await pour l'executer 
           hotelPromise,
           hotelPromise2
           
       ]);
       
       const hotel = hotelSnapshot.data();
       const hotel2 = hotel2Snapshot.data();
       res.send([hotel, hotel2]);
       
   }catch (e){
        
        console.log(e)
    }
    
})
````

***ATTENTION***

Dans les docs il y a beaucoup de .then il faut les transformer en await et async car les docs ne sont pas à jour
