# Reduce

## Fonctionnement d'un opérateur dans un Array

Il y a premièrement les opérateurs qui ne font que parcourir le tableau.

Chaque éléments dans l'ordre va être parcouru.
*Filter*, *ForEatch*

![shemas.reduce](../../assets/shemas.reduce.png)


Ensuite il y a les opérateurs qui fabrique un nouveau tableau. 

Ils créent un nouveau tableau avec symétrie.
*MAP*
On doit absolument mettre un RETURN.
par contre si c'est une petite fonction il n'est pas nécessaire de mettre des acolades.

![shemas.reduce.2](../../assets/shemas.reduce2.png)


Il y a également les opérateurs qui donnent un boléen (true ou false).
*Every* *Some*

et ceux dont on peut avoir plusieurs résultats selon le type de valeur présenté.
*Reduce*

## Reduce
>La méthode reduce() applique une fonction qui est un « accumulateur » et qui traite chaque valeur d'une liste 
(de la gauche vers la droite) afin de la réduire à une seule valeur.

La valeur de départ restera la même que la valeur de sortie. 

Par exemple si currentValue est un Array la valeur de sortie sera un Array.

![shemas.reduce,3](../../assets/shemas.reduce.3.png)
 
![shemas.reduce.4](../../assets/shemas.reduce.4.png)

``````
reduce [(pev,curr) => [ return prev + curr], '' )

````````

>*Une arrow fonction '=>' c'est une fonction*

>reduce, dans ce cas, est une arrow function.

>Une expression de fonction fléchée (arrow function en anglais) permet d’avoir une syntaxe plus courte que les expressions
de fonction et ne possède pas ses propres valeurs pour this, arguments, super, ou new.target. Les fonctions fléchées 
sont souvent anonymes et ne sont pas destinées à être utilisées pour déclarer des méthodes.
