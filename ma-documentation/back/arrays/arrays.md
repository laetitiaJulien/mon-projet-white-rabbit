# Les Arrays

## Qu'est ce qu'un Array ?

>Un Arrays ou tableau est un container qui peut contenir les élèments suivants :
 * des numbers
 * des strings 
 * des booléens 
 * des objets
 * des prédicats
 
```
const = tab [1, 2, 3, ]; 

```

### Définir la longueur du tableau


Pour définir la longueur du tableau, on doit utiliser lenght

````
console.log(lenght);

````

### Pour récupérer un élément du tableau 

Pour récupérer un élément du tableau on utilise des index.
Les index sont attribué à chaque élément. On attribut donc un chiffre en commençant par 0.

````
const tab = [ 1, 2, 13, 80, ];
              0  1   2   3 
console.log(tab.lenght);
                4
````
Dans cette exemple on a bien 4 éléments mais 3 index car on compte à partir de 0

````
const tab = [ 1, 2, 13, 80, ];

console.log(tab[2]);

resultat = 13
````

L'index 2 correspond à l'élément 13

Pour connaitre le dernier élément du tableau 

````
const tab = [ 1, 2, 13, 80];

console.log(tab[tab.lenght -1]

résultat= 4
````

On ne peut additionner des éléments de la même catégorie.

`````
const tab = [ 1, 2, 13, 80];

console.log(tab [1] + tab [0]

résultat = 3
`````

On ne peut additionner que des éléments similaire

si on addittionne un élément avec un élément qui n'existe pas, 
dans la console on verra apparaitre undifined et le résultat sera un NaN (not a number)

``````
const tab = [1, 2, 13, 80];

console.log(tab [2] + tab [5]);
                1       4
résultat = Nan 
``````

## Les opérateurs

### ForEach

>forEach permet d'appliquer une fonction sur chaque élément

`````

const tab [

{firstName: jean , age: 30},
{firstName: Michel , age: 50},
{firstName: elodie , age: 25},

];

tab.forEatch(element => console.log(element.age)

résultat = 30, 50, 25

`````

On peut également appliquer des traitements 

````
const tab [

{firstName: jean , age: 30},
{firstName: Michel , age: 50},
{firstName: elodie , age: 25},

];

tab.forEatch(element => console.log(element.age +10)

résultat = 40, 60, 35
 
````

 appliquer une fonction 
 
 
 ``````        
const tab [

{firstName: jean , age: 30},
{firstName: Michel , age: 50},
{firstName: elodie , age: 25},

];

tab.forEatch(element => console.log(fois deux (element.age));

function foisdeux (x) {
       return x*2
}

résultat = 40, 60, 35

``````

Quand on ne connait pas à l'avance l'attribut que l'utilisateur va sélectionner, on utilise une autre écriture

Exemple nous avons un objet 

```

const user = {
firsName: jean,
lastName: michel,
age: 40,
}

```
On va placer les valeurs possible recherché dans un tableau 

```

const attrs: [firsName, lastName]

```

Puis on va le parcourir pour en ressortir toutes les valeurs 

```
attrs.foreach (attr => console.log(user[attr]));

```

ce qui nous donne (jean, michel) 

### MAP

>parcours le tableau et transforme un élément pour le mettre dans un nouvel élément

exemple ajouter l'index à chaque élément 
 
![capture.map](../../assets/map.png)

pour donner un numéros aléatoire à chaque élément 
on utilise **math** 

![capture.math](../../assets/math.png)

 
### Filter 

>Permet de sélectionner certains éléments par rapport à des critères définis.

Par exemple je souhaite prendre que les personnes de moins de 40 ans.

![capture.filter](../../assets/filter.png)

### Sort

>Permet de réorganiser les éléments dans un tableau 

Par exemple on veut réorganiser les personnes en fonction de leur âge

Pour les comparer il les prend 2 par 2.

si > 0 il le placera devant 

si < 0 il le placera en deuxième 

![capture.sort](../../assets/sort.png)

Par contre pour les strings la fonction ce présente un peu de manière différente
Sort est obligé de tomber sur, soit un nombre positif, soit un nombre négatif 
pour cela on va faire un prédicat.



### Every 

>Permet de vérifier que tous les éléments du tableau corresponde à un critère 

Par exemple que toutes les personnes sont majeur et que la console répond true ou false

![capture.every](../../assets/every.png)

### Some 

>Permet de vérifier qu'un seul des éléments correspond aux critères

![capture.some](../../assets/some.png)

### Push 

>Permet d'ajouter un élément au tableau

![capture.push](../../assets/push.png)




 






 
                
