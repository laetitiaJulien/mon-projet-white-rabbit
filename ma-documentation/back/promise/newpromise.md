# New promise 

## Fabriquer une promesse 

On utilise new Promise qui renvoi une fonction de call back dans lequel il y a deux arguments 
**resolve** et **reject**. Les deux sont des fonctions et non des valeurs. 
- la première fonction va permettre de finaliser la première promesse en donnant la valeur finale
- la deuxème fonctions va rejeter ce que l'ont va mettre dedans c'est ce que l'on retrouve dans le *.catch* 

Exemple : on calcule un nombre rendom de 1 à 10, si le nombre et entre 1 et 5 ça va résoudre la promesse et 
bien nous sortir un nombre entre 1 et 5. 
Si ça fait plus que 5 sa catch une erreur et un message qui nous informe que le nombre est supérieur à 10.

````javascript
app.get('/', function(req, res){
const myPromise = new Promise((resolve, reject)=>{
    const rand = Math.floor(Math.random() * 10 + 1); // permet de générer un nombre entre 1 et 10
    if(rand < 5){
        return setTimeout(()=> resolve(rand), rand * 1000 );// function de call back appelé dés que le timer est fini qui vient en miliseconde 
    } else { 
        return setTimeout(()=> reject(rand), rand * 1000 );
        }
    });
myPromise
    .then((rand)=> {
    console.log(rand);// aprés 5 et 10 secondes je recupére un  nombre entre 5 et 10
    return rand;// on renvoi rand pour send le .then
})
.then((rand)=> res.send(rand + ''))// répondre à l'utilisateur
.catch(e => res.status(500).send(e + ''))
});

````

Pour voir le temps de réponse dans la console cliquer sur Network et dans l'exemple suivant on peut voir 
qu'il a mis environ 11 à 12 secondes pour répondre 

![délai de réponse](../../assets/délairesponse.PNG)


