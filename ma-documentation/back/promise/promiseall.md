# Promise all 

## Comment je traite mes données ?

les fonctions utilisées pour être traité sont asynchrone 
c'est à dire qu'elle vont avoir deux effets :

* Soit le code se joue tout de suite 
* Ou le code se joue avec du retard, ou jamais

les étapes :

Car j'écris dans une base de données 
- J'envoie à un server plus ou moin loin 
- Je lui demande d'écrire ou lire des data 
- Le server doit faire l'opération  
- Ensuite il doit répondre mais si,  par exemple des problèmes de connexion cela peut être plus ou moins long.
C'est ça l'asynchronisme.

Chaque ligne est executée l'une aprés l'autre

````
app.get('/hostels/:id, function (req, res) {
const id = req.params.is;
getHotelById(id)
.then(hostel => res.send(hostel.data()))
.catch(e => res.send(e))

;

})
```` 

le server doit d'abord traiter tout ce qu'il y a avant le '.then' pour exécuter le reste. 

Mais si je ne met que un res.send là se seras synchrone car il n'y a pas de promesse.

````
app.get('/hostels/:id, function (req, res) {
res.send ('ok') 

}
````

Par contre il ne faut pas faire ceci :

````
app.get('/hostels/:id, function (req, res) {
console.log(db.collection('hostels').doc('id de la base'))
res.send ('ok'); 

});

````

car il va commencer par jouer le console.log puis il va lancer l'appelle et sans attendre la réponse va passer au res.send
du coup le console.log ne s'exécutera pas. 

C'est pour cela que lorsque l'on effectue une promesse on utilise un .then et dans le .then on met ce qu'on le veut qu'il se 
passe. Ainsi le server attend la réponse de la fonction avant de passer à la suite et de fermer le tout. 

````
app.get('/hostels/:id, function (req, res) {
db
.collection('hostels')
.doc('id de la base'))
.get()
.then(a) => res.send (a.data())); 

});

````

On utilise ici *a* pour reprendre la fonction. 
Le *data* sert à récupérer l'appel à la base de données mais comme la base de données envoie des informations supplémentaire
(l'heure de la transaction...)c'est pour cela que l'on utilise *.data* 


## Faire deux appels de fonction en même temps 

Si on reprend nos hotels on va demander à la fonction de faire get deux hôtels

pour cela on peut (mais ce n'est pas la méthodes à utiliser) enchainer les .then

![.then](../../assets/chainélesthen.PNG)

c'est un peu compliquer d'utiliser cela surtout quand on veut donner 5 hotels en même temps 


Pour cela on utilise Promise.all 

````
app.get('/hostels/:id, function (req, res) {
const promise1 = db //on stock les données
.collection('hostels')
.doc('id de la base'))
.get()
.then(a) => hostels.push(a.data()))


const promise2 = db //on stock les données
.collection('hostels')
.doc('id de la base'))
.get()
 .then(a) => hostels.push(a.data()))


Promise.all([ //est on envoie les données
promise1,
promise2,
])
.then(result) => res.send(result))// reslt array d'hotel
.catch()
});


````


## Créer des utilisateurs aléatoirement 

````
app.get('/', function (req, res) {
const promiseArray = [];
let i = 0
for (; i < 100; i++) {
promise.puch(db
.collection('test')
.add({value: i}))/je vais chercher dans ma db
} 

Promise.all(promiseArray)/Pour que les promise s'execute
.then(() => res.send(i))
.catch(e => console.log(e))

});

````
Dans ce cas on envoi 100 ordres à la base de données en même temps. Mais la base de données ne les traites pas 
forcément dans l'ordre. 
100 ordres est la limite car le server peut saturé.

Pour  mettre les requêtes dans l'ordre on utilise order. 

## Supprimer plusieurs valeur 

![iddeleted](../../assets/iddeleted.PNG)

Dans les paramètre on notifie bien jusqu'ou on veux supprimer les id 
à la place de add on met .delete
et dans le res.send on pense à typer en mettant 'document deleted'


## Qu'est ce que l'enfer des promesses

On appelle promise hell, les .then qui s'enchaine pour exécuter des promesses les unes après les autres. 

![promisehell](../../assets/promisehell.PNG)
