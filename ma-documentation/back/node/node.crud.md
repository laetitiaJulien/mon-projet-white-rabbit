# CRUD et NODE 


## Récap utilisation node

>Node peut s'utiliser avec ou sans serverless, pour utiliser Node en serverless on peut utiliser des servers mis à disposition 
par des plateformes comme AZUR, AWS, Google cloud plateform. 

### Installer Node sur l'ordinateur

Pour installer Node sur l'ordinateur aller sur le site internet de Node est installer la version 10.16.3.
Pour la faire fonctionner, penser à redémarrer Webstorm.

[installer Node](https://nodejs.org/en/)

Ou regarder dans le terminal si la version de Node est bien la bonne en tapant : node--version 

*Attention* à bien se trouvé à la racine du projet. 

*Pour mettre l'autocomplession* =>
aller dans file 

-> Setting 

-> dans la barre de recherche taper Node 

-> cliquer sur Node.js et npm 

-> puis cocher la case coding assistance for Node.js

*Pour faire du Node* => penser à créer un fichier index.js et une fois la fonction créée
aller dans le terminal et taper node et le nom du fichier index.JS



### Utiliser les npm de node 

>Npm est un gestionnaire de paquets comme Playstore ou Applestore cela permet de télécharger des applications
permettant de faciliter le travail d'un Dév. 
Npm est la version de Node. 

Il en existe d'autre, exemple pour Ruby c'est Jem que l'on utilise

#### Installer Npm 

Aller dans le terminal est taper init. 

Si un fichier 'package.jason' existe déjà c'est qu'il est déjà installé.

##### Package.json 

Dans ce fichier il y a ce qu'on appelle les Devdependencies et les dependencies =

>* les Devdependencies sont les applications utilisées par les dév
(exemple CSS-loader permette de charger les feuilles de style) 

>* les dependencies sont les applications utilisées pas les clients 
(rx.js est utilisé directement dans les applications Front donc directement par le client)

##### Installer une application ou un framework depuis Npm 


Dans le terminal taper npm i (pour installer)

puis -S pour le mettre dans dependencies ou -D dans les Devdependencies

puis le nom du framework (exemple express)


>*Framework* cadre de travaille permettant d'installer des outils de travail 

 
### Utiliser express 

Express utilise des routes pour fonctionner. 

* Chercher des exemples sur internet et les copier coller 
*attention* les exemples utilise souvent les variables VAR penser à le modifier en CONST ou LET

* Créer une variable pour aller chercher le logiciel dont on a besoin =

```

const express = required ('express')

```

* Créer une variable pour utiliser une application 

```
const app = express ()


```

* Pour créer une app 
````
app.get ('/', function (req, res) {

res.send('hello world')


````
Taper localhost pour demander d'écouter les servers.
>* local pour mon ordinateur
>* host pour héberger 

Pour créer une nouvelle route ne se trouvant pas sur la racine 
on va placer /number

**Attention** toujours éteindre sont server en tapant ctrl C 
````
app.get('/number', function (req, res) {
res.send('8')
})
``````

Pour reprendre une fonction 
````
app.get('/number', function (req, res) {
console.log(getHostelsById(25121456))
res.send(getHostelsById(25121456))
})

````
Pour consulter le résultat dans inspecter élément cliquer sur Network -> XHR -> Preview 

Maintenant si on veut récupérer hostels avec son Id
 
Pour cela il faut modifier la route. 

```


app.get('/hotostels', function (req, res) {
const id= parseInt(req.param.ID);
console.log(id)
})
```

### Utiliser Postman 

Installer postman 
Une fois sur Postman choisir soit GET, DELETE... 
Et copier l'url dans la barre de recherche puis SEND . 
Pour créer une nouvelle requête ouvrir une nouvelle page 

Si on reprend l'exemple précédent et que l'on veut utiliser delete 

````
app.delete('/hotostels', function (req, res) {
const id= parseInt(req.param.ID);
console.log(id)
res.send(removeHostels(id));
})

````
si on utilise la même chose mais changeant la requête dans Postman on se met sur l'onglet 
et on fait copier puis on change GET par exemple en DELETE.


                      

## Qu'est ce qu'un CRUD

>*CRUD* =

>* Create on demande à l'API de créer quelque chose **POST**
>* Read on demande à l'API de donner quelque chose **GET**
>* Update on demande de  modifier quelque chose **PUT**
>* Delete pour supprimer **DELETE** 

Le CRUD représente 95% du travail quotidien d'un développeur

### CODE HTTP 

>Un code http permet de renvoyer soit une erreur, soit une information complémentaire  
- les codes 100 la plupart du temps on ne s'en sert pas. 
- les codes 200
status(200) permet de notifier que tout c'est bien passer mais comme c'est le code par 
défaut, on n'est pas obligé de l'écrire, on peut juste mettre res.send

status(201) une ressource a été créée

status(204) une ressource a été créée mais elle n'éxiste pas 

status(205) mettre à jour le contenu 

![status200](../../assets/status.200.PNG)

- les status 400 ont les utilisent le plus couramment de 401 à 404

- les status 500 mon server a planté 

Pour les autres se référer au lien en dessous :

[status](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP)

## Particularité PUT et POST 

###POST 

Un post ce fait toujours à la racine d'un projet puisque qu'on envoie une donnée

- Dans app.post mettre la racine du projet 

```
app.post('/hostels', functions (req, res)


)
```

- Mettre le status 201

```
app.post('/hostels', functions (req, res)
res.status(201)

)
```
- Dans POSTMAN dupliquer une requête et mettre POST 

- Ensuite copier l'hotel que l'on veut depuis Webstorm 

- Revenir sur POSTMAN, sélectionner body puis raw puis package.json

![body de POSTMAN](../../assets/bodyandrow.png)

- Dans le body de POSTMAN copier l'hotel

**Attention** tous les strings doivent être entre des doubles quotes. 

![hotel dans POSTMAN](../../assets/POSTMANdoublequotes.png)

- Récupérer le body de POSTMAN

```
app.post('/hostels', functions (req, res) {
const createdHostel = req.body
res.status(201).send(createdHostel);

});
```


- Pour cela il faut penser à mettre en place d'autre fonctionnalité.
 
En haut de la feuille penser à mettre :

```
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

```
- Maintenant appeler la fonction qui appelle l'hotel 

```
app.post('/hostels', functions (req, res) {
const createdHostel = req.body;
addHostels(createdHostels);
res.status(201).send(createdHostel);

});
```
- Dans la fonction que l'on avait initialement créée on reprenait l'id de l'hotel
pour cela il faut donc ajouter

```
app.post('/hostels', functions (req, res) {
const hostelToCreate = req.body;
const createdHostels = addHostels(hostelToCreate);
res.status(201).send(createdHostel);

});
```
Dans ce cas on va passer hostelToCreate dans le body puis on va rappeler 
hostelsToCreate dans la fonction addHostels

### Put 

- Dans POSTMAN dupliquer le POST et mettre PUT 

- Ensuite créer la route et ajouter l'id

``
app.put('/hostels/:id', functions (req, res) {
const hostelToCreate = req.body;
const id = req.params.id
const hostelToCreated = putHostel (id, hostelsToCreate)
res.send(hostelToCreated)
});
```
pas besoin de status particuliers pour PUT 

