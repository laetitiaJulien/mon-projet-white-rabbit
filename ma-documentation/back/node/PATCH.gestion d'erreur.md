# PATCH et Gestion d'erreur 

## PATCH

>PATCH permet de modifier les clés d'un objet avec un autre 

**Attention** les clés utilisées doivent être autorisées c'est-à-dire initialement prévue 
dans l'objet. 

- La première chose à faire c'est vérifier et identifier les clefs de l'objet 
pour cela on utilise un findIndex
 ````javascript
app.patch('/hostels/:id', functions (req, res) {
const hostelToCreate = req.body;
const id = req.params.id
const index =  hostels.findIndex(value => value.id === id);

});
 
 ````
- Pour vérifier que l'ont à bien récupéré le bon objet, on créé une const hostelTo change
et on fait un console.log de cette const

````javascript

app.patch('/hostels/:id', functions (req, res) {
const dataToChange = req.body;
const id = parseInt(req.params.id)
const index =  hostels.findIndex(value => value.id === id);
const hostelsToChange = hostels[index];
res.send(hostelsToChange);
});
`````
- Maintenant on va récupérer les clefs 

````javascript
const dataToChange = req.body;
const keysToChange = Object.keys(dataToChange);
const id = parseInt(req.params.id)
const index =  hostels.findIndex(value => value.id === id);
const hostelsToChange = hostels[index];
res.send(hostelsToChange);
});
````
Pour vérifier que clefs de l'objet que l'on veut changer soit autorisées
on va créer une autre const 

````javascript
const dataToChange = req.body;
const keysToChange = Object.keys(dataToChange);
const id = parseInt(req.params.id)
const index =  hostels.findIndex(value => value.id === id);
const hostelsToChange = hostels[index];
const authorizedKeys = Object.keys(hostelsToChange);
res.send(hostelsToChange);
});
````
Pour vérifier que l'on a le droit de voir la première clef on fait une boucle 

````javascript
const dataToChange = req.body;
const keysToChange = Object.keys(dataToChange);
const id = parseInt(req.params.id)
const index =  hostels.findIndex(value => value.id === id);
const hostelsToChange = hostels[index];
const authorizedKeys = Object.keys(hostelsToChange);
const isauthorised = authorizedKeys.includes(keysToChange[0]);
console.log(isauthorised)
res.send(hostelsToChange);

});
````
>**include** permet de demander si l'objet entre parenthèses appartient bien au tableau.
Si on l'obtien true on est sur qu'il est bien à l'intérieur

Maintenant que l'on a vérifié qu'une seule clef est autorisée, il faut modifier cette 
boucle est le faire sur toutes les clefs

 ````javascript
const dataToChange = req.body;
const keysToChange = Object.keys(dataToChange);
const id = parseInt(req.params.id)
const index =  hostels.findIndex(value => value.id === id);
const hostelsToChange = hostels[index];
const authorizedKeys = Object.keys(hostelsToChange);
const isauthorised = 
keysToChange.map(key => authorizedKeys.includes(key));
console.log(isauthorised)
res.send(hostelsToChange);

});
````

Si l'une des clés est false donc *isok* = false, il faut le mettre en évidence 

````javascript
const dataToChange = req.body;
const keysToChange = Object.keys(dataToChange);
const id = parseInt(req.params.id)
const index =  hostels.findIndex(value => value.id === id);
const hostelsToChange = hostels[index];
const authorizedKeys = Object.keys(hostelsToChange);
const isauthorised = 
keysToChange.map(key => authorizedKeys.includes(key));
const isok = isauthorised.every(key => key)
res.send(hostelsToChange);

});
````

- Maintenant que l'on a un retour comme quoi une des clés est bien false, il faut envoyer 
un message d'erreur 
Pour renvoyer une erreur on utilise *throw*

````javascript
 const dataToChange = req.body;
   const keysToChange = Object.keys(dataToChange);
   const id = parseInt(req.params.id)
   const index =  hostels.findIndex(value => value.id === id);
   const hostelsToChange = hostels[index];
   const authorizedKeys = Object.keys(hostelsToChange);
   const isauthorised = 
   keysToChange.map(key => authorizedKeys.includes(key));
   const isok = isauthorised.every(key => key)
   if(!isOk) {
   throw new Error('non authorized key');
   }
   console.log(isok)
   res.send(hostelsToChange);

});

````
## Gestion d'erreurs

- Le message d'erreur est bien apparu mais pas de manière clair puisque cela plante le programme 
Pour ne pas planter le programme on utilise 
>*try* = essaye de faire quelque chose 

>*Catch* = si ça se passe mal fait quelque chose 

````javascript
try {
   const dataToChange = req.body;
   const keysToChange = Object.keys(dataToChange);
   const id = parseInt(req.params.id)
   const index =  hostels.findIndex(value => value.id === id);
  let hostelsToChange = hostels[index];
   const authorizedKeys = Object.keys(hostelsToChange);
   const isauthorised = 
   keysToChange.map(key => authorizedKeys.includes(key));
   const isok = isauthorised.every(key => key)
   if(!isOk) {
   throw new Error('non authorized key');
   }
   res.send(hostelsToChange); 
} catch (e) {
  console.log(e.message);
res.status(400).send(e.message);
  }

````
le *e* va permettre de laisser un message clair en plus du code erreur 400.

- Dans le cas ou *isok* = true, les clefs que l'on veut changer sont bonne mais du 
coup comment changer l'objet ?
On va reprendre tout ce qu'il y a dans hostelToChange et mixer ça 
avec dataTochange 

````javascript
try {
   const dataToChange = req.body;
   const keysToChange = Object.keys(dataToChange);
   const id = parseInt(req.params.id)
   const index =  hostels.findIndex(value => value.id === id);
  let hostelsToChange = hostels[index];
   const authorizedKeys = Object.keys(hostelsToChange);
   const isauthorised = 
   keysToChange.map(key => authorizedKeys.includes(key));
   const isok = isauthorised.every(key => key)
   if(!isOk) {
   throw new Error('non authorized key');
   }
   hostelsToChange = {...hostelsToChange, ...dataToChange}
   putHostels (id, hostelsToChange);
   console.log(hostels)
   res.send(hostelsToChange); 
} catch (e) {
  console.log(e.message);
res.status(400).send(e.message);
  }

````
*Object.Assigned* = prendre un objet pour changer ses clés 

````javascript
try {
   const dataToChange = req.body;
   const keysToChange = Object.keys(dataToChange);
   const id = parseInt(req.params.id)
   const index =  hostels.findIndex(value => value.id === id);
  let hostelsToChange = hostels[index];
   const authorizedKeys = Object.keys(hostelsToChange);
   const isauthorised = 
   keysToChange.map(key => authorizedKeys.includes(key));
   const isok = isauthorised.every(key => key)
   if(!isOk) {
   throw new Error('non authorized key');
   }
   Object.assign(hostelsToChange,dataToChange );
   /*putHostels (id, hostelsToChange);*/
   console.log(hostels)
   res.send(hostelsToChange); 
} catch (e) {
  console.log(e.message);
res.status(400).send(e.message);

````
