# Introduction à Node 

## Définitions

*Le Node* permet de faire du server less, c'est à dire de ne pas passer par un server. 
Par contre, Node.js offre un environnement côté server qui nous permet aussi d'utiliser 
le langage JavaScript pour générer des pages web. En gros, il vient en remplacement de langages server comme PHP, Java EE, etc.

Node va pouvoir communiquer avec une Interface API  

## Qu'est ce que qu'une API et un API REST

*API* signifie Application Programming Interface. Le mot le plus important est “interface”, 
et c’est le mot le plus simple, car nous utilisons tous des interfaces au quotidien ( exemple API de paypal).

*API REST* signifie “Representational State Transfer”.Les API REST sont basées sur GIT ou HTTP, qui signifie Hypertext Transfer Protocol.
C’est ce qui est au cœur du web ! C’est un protocole qui définit la communication entre les différentes parties du web. 
L’échange est basé sur des requêtes client et server.
Un client lance une requête HTTP, et le server renvoi une réponse.
Ce sont des méthodes qui définissent les requêtes que le client peut effectuer, dont GET, PUT, POST, DELETE 

*Protocole d'échange* Un protocole informatique définit les règles et les procédures permettant à deux processus informatiques d’échanger des données, 
notamment à travers un réseau.

## Quelle sont les différentes requête


- **GET** Donner (donne moi tel ou tel information)
- **PUT** modifie ou remplace part...
- **DELETE** supprime 
- **POST** créé   


## Outil pour interroger une API


Postman permet de construire et d’exécuter des requêtes HTTP, de les stocker dans un historique afin de pouvoir les rejouer, 
mais surtout de les organiser en Collections. Cette classification permet notamment de regrouper des requêtes de façon « fonctionnelle » 
(par exemple enchaînement d’ajout d’items au panier, ou bien un processus d’identification). 




