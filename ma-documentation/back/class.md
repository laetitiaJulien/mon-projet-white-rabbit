# class

## Qu'est ce que les Classes ?

>C'est un outil qui à été créé aprés le démarrage de l'informatique pour aider les développer à conceptualiser 
des choses complexe en code.

>Cela permet de mettre en forme et de mieux comprendre le code surtout quand il y a des centaines de données.

>Le système de class est trés utile surtout quand on se sert de ce que l'on appelle l'héritage.

**Héritage** 

Les objets JavaScript sont des ensembles dynamiques de propriétés (les propriétés directement rattachées
à un objet sont appelées propriétés en propre (own properties)). Les objets JavaScript possèdent également 
un lien vers un objet qui est leur prototype. Lorsqu'on tente d'accéder aux propriétés d'un objet, 
la propriété sera recherchée d'abord sur l'objet même, puis sur son prototype, puis sur 
le prototype du prototype et ainsi de suite jusqu'à ce qu'elle soit trouvée ou que 
la fin de la chaîne de prototype ait été atteinte


## Dans la pratique 

Pour déclarer une class on nomme tout simplement class.
Comme un objet on peut lui mettre des propriétés 

![class exemple](../assets/ex1class.PNG)

Par contre une class à un constructeur en plus 

>**Le Constructeur** c'est une fonction dont le nom est imposé et 
>qui est destiné à créer une instance d'une class

![constructeur](../assets/constructeur.PNG)


>**Une Instance** reprend les mêmes caractéristiques qu'une class mais de manière différente.

En programmation orientée objet, on appelle instance d'une classe un objet avec un comportement et un état, tous deux définis par la classe. Il s'agit donc d'un objet constituant un exemplaire de la classe. Dans ce contexte, instance est un anglicisme, qui signifie « cas », « exemple ».

L'instanciation est l'action d'instancier, de créer un objet à partir d'un modèle. Elle est réalisée par la composition de deux opérations : l'allocation et l'initialisation. **L'allocation** consiste à réserver un espace mémoire au nouvel objet. **L'initialisation** consiste à fixer l'état du nouvel objet. Cette opération fait par exemple appel à l'un des constructeurs de la classe de l'objet à créer. La modification de ces opérations permet de réaliser la réflexion structurelle.

En programmation orientée classe, l'instanciation est la création d'un objet à partir d'une classe. En programmation orientée prototype, l'instanciation consiste à créer un nouvel objet à partir d'un objet existant (clonage). 

ex : class = être humain instance = tom cruse ou mon voisin de palier 

Pour créer une instance on créer une const que l'on utilise dans le constructeur ensuite. 

![instance](../assets/instance.PNG)

Pour utiliser le constructeur on va par exemple utiliser id pour le récupérer. 
On utilise ensuite le mot clé .this pour manipuler l'instance.

![this](../assets/this.PNG)

Ensuite on appel l'id dans la class qui se trouve dans notre code. 

![appelconstructor](../assets/appelconstructor.PNG)
  
Pour reprendre maintenant l'ensemble de l'HotelModel on va assigner l'objet et prendre l'instance de la class.

![assign](../assets/assigne.PNG)

Quand on utilise le système des class on peut utiliser des fonctions spéciales
exemple: recalculer la taille de roomNumbers.

![calculator](../assets/function%20calculator.PNG)
  
et dans la fonction on lui ajoute bien rooms et on rappelle la functions.

![functioncalc](../assets/functioncalc.PNG)

On peut également créer des méhodes (fonction) qui permette de détruire ou de créer quelque chose.

![destroypool](../assets/destroy%20pool.PNG)

Pour que dans le code on puisse appeler une fonction deux fois sans faire ceci ;

![deux functions](../assets/2functions.PNG)

il faut que dans la classe on renvoi this. 

![return this](../assets/return%20this.PNG)

![return this](../assets/returnthiscode.PNG)

### Créer des sous class HotelClass

Une sous class reprend ce qu'il y a dans la classe parent mais avec des choses en plus.
Pour ne pas tout réécrire on signifie que c'est une extension de la class parent et on ajoute ce qu'il y a de nouveau.
On récupère ainsi le constructeur et les méthodes en même temps

![sous class](../assets/sousclass.PNG)

![luxeHotel](../assets/luxeHotel.PNG)

Pour ajouter des éléments au constructeur parent, on utilise le mot clé super() dans le constructeur de l'extension (sous class).

![modifconstructeur](../assets/modifconstructeur.PNG)
![modifconstructeur 2](../assets/modifconstructeur2.PNG)


## Faire la même chose avec RoomModel

**implements** force a reprendre le même model.

Pour implementer automatiquement roomModel on clique sur roomClass et on clique sur **generate**

![generate](../assets/generate.PNG)

Ensuite on clique sur constructor

![generate constructor](../assets/generateconstructeur.PNG)

Puis on selectionne toutes les clés.

![generate keys](../assets/generate2.PNG)


![shemas class](../assets/Capture%20d’écran%202019-12-04%20à%2015.16.13.png)


