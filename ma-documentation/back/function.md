# Les fonctions

## Qu'est qu'une fonction ?

>Une fonction sert à déclencher des ordres.


## Comment s'écrit une fonction ?

- Tout d'abord on nomme la fonction:

````
function produit

````
- Ensuite on met des paranthèses pour y mettre des paramètres.

````
function produit (x, y)

````

- Ensuite on met des accolades dans lequel la fonction va s'exécuter.

````
function produit (x, y) {

return ....

}

`````


**Une fonction qui ne renvoie rien (sans return), est appelée return void**

Si une fonction **return** quelque chose on va faire un const ou let 

````

function coucou () {

return 42

}

const toto = coucou()

console.log(toto)

result = 42

````


*les parenthèses sont obligatoire quand on appelle une fonction*



## Les paramètres 

Les paramètres permettre de donner des critères aux fonctions

````

function produit (x, y) {

return x * y;

}

console.log(produit(x:2, y:10));

````

On peut techniquement mettre autant de paramètre que l'on veut dans une fonction MAIS la norme impose 4 paramètres maxi. 
Pour des questions de praticitée 

Quand on ne sait pas combien d'argument on va mettre dans sa fonction un peu utiliser ...arg
ou le dernier argument de la fonction ...z
Cela va prendre tous les arguments de la fonction et les placer dans un Array. 
C'est un rest operator

````
function randomAction (...z) {

console.log(z){
z.map(value => console.log(value)) 
}
console.log(produit(x:2, y:10));

````

On peut également ajouter des paramètres avec le rest opérator

````
function randomAction (a, b, c, ...z) {

console.log(a + b + c, z)

}

randomAction( a:1, b:20, c:3, z:10)

resultat = 24 [10]

````

Le rest operator doit toujours être écrit en dernière position dans les paramètres.




## Mettre un prédicat dans une fonction

On peut également effectuer des prédicats.

````
function randomAction (x, y) {

if (x < 10) {
    return x * y;
    }
if (y > 20 ) {  
    return x - y;
    }
}

console.log(randomAction(x:2, y:10));

resultat = 20

````


La fonction va toujours s'arrêter au premier return sans exécuter le reste du code.

````
function randomAction (x, y) {
    return 0;
if (x < 10) {
    return x * y;
    }
if (y > 20 ) {  
    return x - y;
    }
}

console.log(randomAction(x:2, y:10));

résultat = 0

`````

Si aucun des deux **if** ne fonctionnent il faut penser à mettre quand même une valeur de retour
car il ne faut pas avoir undifined dans la console.
La valeur de retour doit être mise à la fin.

`````
function randomAction (x, y) {
if (x < 10) {
    return x * y;
    }
if (y > 20 ) {  
    return x - y;
    }
    return 0;
}

console.log(randomAction(x:2, y:10));

`````

 
