# Firebase functions

## TypeScript 

>TypeScript est un superset de java script, il permet de lui donner plus de puissance. 
Il permet de **transpiler** un language a => en language b.

Le but est de mettre en place les functions de Typescipt, est de pouvoir faire du back avec les serverless. 

Avec TypeScript le système se met en place tout seul.

C'est une façon de faire mais il y en a d'autre.

### Mettre en place TypeScript 

- Créer un dossier tsSrc pour faire un fichier TypeScript dans ce dossier 


- Ouvrir le terminal et vérifier que l'on se trouve bien à la source du projet

- Dans le terminal taper cd tsSrc *cd = change directory*

![creat tsSrc](../../assets/1.TS.png)


- Une fois le dossier sélectionné installer les fonctions de firebase. 

- Toujours dans le terminal taper: firebase init functions

![init functions](../../assets/4.%20TS.png)

ceci apparait donc 

![firebase](../../assets/4.TS%202.png)

- Webstorm demande ensuite si l'on est prêt à démarrer. Taper Y pour yes

- Ensuite choisir parmi la liste: 

on nous propose d'utiliser soit un projet existant
 
soit en créer un et d'autres possibilité. 

- Avec les flêches du haut et du bas choisir: use an existing project. 

![use an existing project](../../assets/7.TS.png)

- Sélectionner le nom du projet. 

- Sélectionner TypeScript comme language.

![TypeScript](../../assets/8.TS.png)

- Webstorm propose ensuite d'utiliser TSlint, cela permet d'avoir un code propre. Taper Y pour yes

![TSlink](../../assets/9.%20TS.png)

- La question suivante concerne les dépendencies, on répond Y 

![dependencies](../../assets/10.TS.png)

- Une fois l'intallation des dépendencies faite, cliquer droit sur le dossier tsSrc
et synchroniser pour mettre à jour les nouveaux fichiers

![synchronisation](../../assets/11.TS.png)

- Une fois le dossier à jours un autre dossier Functions va apparaitre. 

Les fichiers qui seront dedans seront en rouge car ils ne sont pas reliés à Git, 
donc cliquer droit à nouveau sur tsSrc est sélectionner Add.

Les fichiers passeront en vert.

![Add](../../assets/12.TS.png)
![Add2](../../assets/12.TS2.png)

- Dans le dossier functions on trouve un autre dossier src avec un fichier index.ts. 

C'est dans ce dernier qu'il faudra coder.

- Ensuite cliquer droit sur package.json (celui de tsSrc), et sélectionner Show npm Script. 

![show npm Script](../../assets/14.TS.png)

- Une nouvelle fenêtre de commande va apparaitre, mais la commande qui nous intéresse c'est serve

**serve** est un script npm qui va fabriquer un TypeScript et le tanspiler en javaScript.

Ce qui va permettre d'executer l'index.ts et permet de le jouer dans le terminal en cliquant dessus 
(au lieu de faire ctrl + c et de taper le nom du fichier).


## Vérifier que TypScript fonctionne 

- Dans le fichier index.ts faire un console.log de coucou et cliquer sur serve.
le terminal va lancer un server (ex 5000) plus besoin de faire : 

```
app.listen (5000) {
    console.log ()
}
```

Une Url est donner dans le terminal est permet de voir que le server fonctionne bien.

- En node on utilisait le required pour importer quelque chose.
 
 En TypeScript on utilise le mot import *.
 
 Noter au début l'import de functions 

```
import * as function from 'firebase-functions';

```
- Ensuite faire une fonction (ex 'hello world') :

```
export const helloWorld
```
- Ensuite uiliser functions :

```
export const helloWorld = functions
```

- Et la fonction que l'on va utiliser sera https pour fabriquer un server http.

Le s signifie sécurisé.

```
export const helloWorld = functions.https
```

**functions** sont des minis server node avec plein de fonctionnalités.

- Ensuite utiliser le onRequest pour avoir une réponse 

```
export const helloWorld = functions.https.onRequest(request, response) =>
```

- Pour envoyer la réponse on va utiliser send, ce qui nous fait au finale cette ligne de code 

```
export const helloWorld = functions.https.onRequest(request, response) =>{
response.send("hello from firebase!");
});
```
- Pour relancer le server appuyer à côté du terminal le bouton flêche verte.
 
 Pour voir le résultat cliquer sur le lien donner dans le terminal. 

![serve](../../assets/serve.png)

![url hello world](../../assets/url%20hello-world.png)

#### Modification du package.json

La modification du package permet de prendre en compte chaque changement du code automatiquement 

- Cliquer sur le package.json de tsSrc 

- Placer son curser au niveau de "build" est dupliquer la ligne (ctrl + D)

- Faire une nouvelle commande "watch": "tsc -w"

![functions watch](../../assets/watch.png)

**tsc** = transpile
**-w** = prend en compte les changements et recompile

- Cliquer dans le fenêtre de commande sur les flèches rafraichir reloadscript et double cliquer sur watch 

serve et watch tourne en parallèle.

## Installer express

- Dans le terminal taper: cd functions 

![cd functions](../../assets/cd-functions.png)

- Ensuite taper: npm i -s express

![npm i](../../assets/functions%20i%20-s%20express.png)

- Ouvrir package.json et vérifier que la version de Express soit installée 

- Importer express sur TypeScript, taper dans le fichier index.ts

```
import * as express from "express"; 
const app = express()
app.use(express.json())
app.use(express.urlencoded({extended: true}));
```
- Une fois cette installation terminée on peut commencer à coder et donc mettre des fonctions 

**Exemple** 

```
app.get('/', function (req, res){
    res.send('hello world')
});

export const test = functions.https.onRequest(app);
```

## Apprendre à faire du code TypeScript proprement

>Le mot Type dans TypeScript veut dire que l'on va déclarer les types de nos data.

- Créer un nouveau dossier dans tsSrc => src appeler models

![new directory](../../assets/new%20directory.png)

- Dans ce dossier on va créer deux files 

    - hotel.model.ts
    - room.model.ts

- Quand on créé des **models** on commence par l'objet le plus petit, ici les rooms. 

Donc dans le fichier room.model.ts on copie une seul room (n'importe laquelle)

- Puis mettre **export interface roomModel**. 

```
{
    roomName: 'suite canoise'
    size: 2
    id: 4
}

export interface RoomModel 
```

>**export** pour permettre de déclarer soit une fonction soit une constante on pourra y accèder depuis un autre fichier.

>**interface** c'est la définition de ce qui a dans le modèle 

Webstorm va automatiquement reprendre le nom du fichier(roomModel).

- Dans ce roomModel on à plusieurs types (string et numbers)

```
{
    roomName: 'suite canoise'
    size: 2
    id: 4
}

export interface RoomModel {
    roomName : string;
    size: number;
    id: number; 

}
```

- Puis effacer l'exemple et garder que export

![roomModel](../../assets/roomModel.png)

- Faire la même chose avec hotel.model.ts mais attention à room qui est un array de roomModel. Automatiquement Webstorm 
va importer sur le fichier hotel.model.ts ce qui ce trouve dans room.model.ts. 
Si, il ne le fait pas automatiquement faire alt + entrée sur le type rooms: RoomModel pour que le ligne import se mette.
Mais vue que c'est un array penser à placer des [] .

![hotelModel](../../assets/hotelModel.png)

- Faire un troisième fichier que l'ont place directement à la racine (src)
appelé database.data.ts. 

Dans ce fichier on va coller le hostels en entier. 

![troisième dossier](../../assets/thirddirectory.png)


- Changer le let en const et mettre le type donc hotelModel et [] pour indiquer que c'est un array. 

- Créer un fichier avec les functions appelé utilities.ts 
puis copier coller les functions dedans. 

- Tous ce qui se trouve en rouge, il faut le typer et mettre export devant chaque fonction pour pouvoir les utiliser 

- Dans une fonction il peut y avoir des models attention à bien les spécifier aprés les paramêtres.
 
Si on ne sait pas qu'elle models il y aura, 
faire alt + entrée sur le nom de la fonction et cliquer sur specify type explicitly et entrée. 

![specify](../../assets/specify.png)











