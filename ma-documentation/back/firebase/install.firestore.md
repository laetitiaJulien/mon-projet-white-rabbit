# firestore

## Installer la base de données 

Pour l'installer il faut se référer à la documentation de firebase 

[firebase documentation](https://firebase.google.com/docs/firestore?authuser=0)

- Aller sur firebase et entrer sur la console : 

![firebase](../../assets/firebaseconsole.PNG)

- Sélectionner le projet en cours 

![selectionduprojet](../../assets/selectionduprojet.PNG)

- Ensuite accéder à la documentation en haut à droite de la page

![accederàladoc](../../assets/accederaladoc.PNG)

- Cliquer sur get started for admin pour lire la doc (admin pour back-end)

![getstartedforadmin](../../assets/getstartedadmin.PNG)

- Choisir sur la colonne de gauche cloud functions 

![cloudfunctions](../../assets/cloudfunctions.PNG)

- Puis Get started (on doit entrer un token d'authenfication pour configurer l'ordinateur)

![getstarted](../../assets/getstarted.PNG)

>**Token d'authenfication** fichier texte servant au server de nous reconnaitre

- Aller dans Test functions

![test functions](../../assets/test%20functions.png)

et regarder dans run functions localy pour avoir le process en entier 

le process est le suivant 

- Revenir dans sur la page d'accueil et cliquer en face de project sur les paramètre 
du projet
 
![paramètre projet](../../assets/parametreprojet.PNG)

- Sélectionner compte de service puis gérer les comptes de services 

![compte de service](../../assets/gérer%20les%20comptes%20de%20service.PNG)

- Une autre page s'ouvre et on arrive sur Google Cloud Platform: 
là séléctionner à nouveau compte de service 

![compte de service2](../../assets/compte%20de%20service.PNG)

- Sélectionner tout en haut le projet si celui-ci n'est pas déjà sélectionné 

![sélection du projet](../../assets/selectionduprojet.PNG)

- Cocher la première case pour sélectionner le projet dans App default service account 

![cocher la case projet](../../assets/cocherlacase.PNG)

- Sur la droite cliquer sur les 3 points et cliquer sur créer une clé

![créer clé](../../assets/créerkey.PNG)

- Une fenêtre doit s'ouvrir avec la case Json pré-cocher puis cliquer sur créer

![prompt json](../../assets/json.PNG)

Une clé doit être téléchargée en bas du navigateur

Maintenant que cette clé est téléchargée on va la mettre à la racine du projet.

**Attention** ceci est valable uniquement sur ce projet, une fois en entreprise 
il ne faudra pas mettre la clé à la racine du projet. Une clé doit être propre à chacun 
sur un ordinateur et non dans le Git d'un projet. 

- Ouvrir le fichier de la clé téléchargée puis cliquer sur afficher le dossier

![afficher le dossier](../../assets/afficherle%20dos.PNG)

- Un dossier s'ouvre avec le téléchargement récent, cliquer sur le fichier de la clé
et le copier coller à la racine du projet Webstorm, c'est-à-dire le dossier functions

![dossierclé](../../assets/dossierclé.PNG)

![racine du projet](../../assets/racineprojetclé.PNG) 

- Renommer le fichier en key.json

![rename key](../../assets/renamekey.PNG)

- Retourner dans la doc et copier la fonction en précisant windows

![set key](../../assets/windowsgooglekey.PNG)

- Dans le terminal taper set puis coller la ligne en finissant par ./key.json

![key dans le terminal](../../assets/windowskeyjson.PNG)

Pour accéder à la base de donnée 

- Dans le typescript (index.ts) tout en haut en dessous de **functions from firebase** 
taper ceci 

![importfirebase](../../assets/importadminfirebase.PNG)

- Puis en dessous c'est deux lignes pour initialiser le server

![initialise](../../assets/initialize.PNG)

- faire une copie de key.JSON et le mettre dans src 

- modifier le code de cette manière pour reprendre le service account

![service account](../../assets/sercieaccount.PNG)

- Retourner sur firebase est sélectionné database 

![database](../../assets/database.PNG)

- Cliquer sur créer base de données 

![créer database](../../assets/créerbase.PNG)

- Une pop-up s'ouvre puis cocher sur commencer en mode test 

![commencer en mode test](../../assets/starttest.PNG)

- Cliquer sur suivant et sélectionner la zone europe 

![zone europe](../../assets/zoneue.PNG)

Dans les base de données on a des *collections* qui correspond à un tableau 
et des *documents* c'est ce qui se trouve dans le tableau 

## faire un test 

- Dans le fichier index.ts reprendre hostels, indiqué un identifiant test et 
une donnée 
on va chercher les données dans une collection 
après on peut directement travailler dans la collection ou alors dans la doc aussi.
Ensuite on utilise un verbe d'action :
 (dans ce cas on utilise set).

**add** comme push pour un array ça va pusher la donnée dans la collection 

**delete** ça supprime une donnée de la collection 

**set** modifie et remplace la donnée dans la collection 

**update** s'utilise avec patch 

**get** donne un élément 

![test](../../assets/test1.PNG)

- Puis relancer le server créer une commande emulator. Pour cela aller dans package.json
et taper ceci en dessous de start 

![emulator](../../assets/emulator.PNG)

- Ensuite cliquer sur reload pour que la commande apparaisse et cliquer dessus pour la
lancer

![reload](../../assets/reload.PNG) 

- Cliquer sur le lien qui apparait dans le terminal pour voir apparaitre le résultat

![result de emulator](../../assets/resultemulator.png)

- Aller dans tsconfig.json et écrire ceci 

![resolve](../../assets/resolve.PNG) 

- Ensuite penser à mettre c'est ligne de code 

![lignecodekeys](../../assets/lignecodekey.PNG)

- A la function db rajouter le .then 

![then](../../assets/then.PNG)

le .then permet d'effectuer tout ce qui y a en haut de la fonction pour après de lancer 
ce qu'il y a à la suite du .then c'est ce que l'on appelle une promesse


- Ensuite on rajoute catch pour gérer les erreurs 

![catch](../../assets/catch.PNG)










 
