# Boucle For

## Qu'est ce qu'une boucle For 

>C'est un outil qui permet de faire des boucles sur des éléments données. 

Elle est composé de 2 parties:

- le première partie entre parenthèse est dite conditionnel.
elle exprime dans quels conditions la boucle va tourner.

Cette partie est composée de trois sous partie optionnel 

1. partie d'initialisation (expression de code qui est évalué au démarrage 
de la boucle for) optionnel.

2. quel que soit les conditions on va continuer à jouer la boucle for. Renvoie un prédicat
false = elles s'arrête.
true = elle continue à l'infinie. 
Pas besoin d'avoir un rapport avec la boucle.

3. code éxécuté à chaque fois que la boucle est fini. (optionnel)

- Et entre crocher c'est la partie qui va exécuter ce qui ce trouve dans la partie 
conditionnel 


```
for(-------------- ; ----------- ; ------------- ){
    initialisation   conditions     fin de code

}


*i++* : i = i + 1 différent de i + 1  
*i--* : i = i - 1 différent de i - 1
*i+=2* ==> i = i + 2
```
## Exemple 

Par exemple on voudrait que dans cette const les noms s'affiche 

```
const names = ['dupont', 'durand', 'benyassine', 'oudoul']

```


on va créer une boucle For avec un let i qui commence à 0


```
for (let i = 0; ){

}
```

On ajoute une condition où tant que i est inférieur a names.length (3) on continue

```
for (let i = 0; i < names.length; ){

}
```

et i++ pour rajouter 1 à la i = 0

```
for (let i = 0; i < names.length; i++ ){

}
```

puis je rajoute un console.log de names i

```
for (let i = 0; i < names.length; i++ ){
    console.log(names[i]) 
}
```

Le mieux est d'évité de mettre un nombre en dur à la place de length car si on 
ajoute un nom dans la liste et que par exemple on met i< 3, il ne nous mettra que
les 3 premiers nom.
````
for (let i = 0; i < 3; i++ ){
    console.log(names[i]) 
}
````

Pour afficher un nom sur deux 

``
for (let i = 0; i < 3; i+=2 ){
    console.log(names[i]) 
}
``

Pour afficher durand et oudoul on commence à 1

``
for (let i = 1; i < names.length; i+=2 ){
    console.log(names[i]) 
}
``

## Exemple générer des users

- On veut créer un tableau avec des users 

````javascript
function randomize(start, end) {
    return Math.floor((Math.random() * end + start))
}
const lastNames = ['dupont', 'durand', 'benyassine', 'oudoul']
const firstNames = ['Yasinne', 'Nicolas', 'Kévin', 'Jeanne']



const users = []

for (let i = 1; i < 10; i++ ){
    users.push({lastNames : lastNames[randomize(0, 4)],
    firstNames: firstNames[randomize(0.4)]
    })
}

console.log(users)
````

Ce qui nous donne 

![boucleforarray](../assets/arraybouclefor.PNG)

- Maintenant on veut ajouter un age à ces users

````javascript
const lastNames = ['dupont', 'durand', 'benyassine', 'oudoul']
const firstNames = ['Yasinne', 'Nicolas', 'Kévin', 'Jeanne']

const users = []

for (let i = 1; i < 10; i++ ){
    users.push({lastNames : lastNames[randomize(0, 4)],
    firstNames: firstNames[randomize(0.4)],
    age: randomize(0, 99)
    })
}
console.log(users)
````
- On ajoute ensuite un code postale

````javascript
const lastNames = ['dupont', 'durand', 'benyassine', 'oudoul']
const firstNames = ['Yasinne', 'Nicolas', 'Kévin', 'Jeanne']

const users = []

for (let i = 1; i < 10; i++ ){
    users.push({lastNames : lastNames[randomize(0, 4)],
    firstNames: firstNames[randomize(0.4)],
    age: randomize(0, 99),
    zip: randomize(10000,99000)
    })
}
console.log(users)

````
ce qui nous donne 

![bouclezip](../assets/zipboucle.PNG)

## Modulo 

un modulo (%) permet de savoir si un nombre est divisible ou pas 

0 = est divisible sans virgule
1 = n'est pas divisible 

