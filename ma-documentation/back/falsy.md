# Falsy

>Les falsy sont des valeurs équivalente à false.

les falsy sont à connaitre car cela évite les erreurs.

***IMPORTANT*** Tout ce qui n'est pas falsy est trucy.

## La liste des falsy est la suivante:

* false
* 0
* '' (string vide, si on met un espace dedans il n'est plus vide)
* null
* undifined
* NaN

**exemple** 
 
Si sur une page internet il y a un espace commentaire
et que le nombre de commentaire est égale à 0.
Rien ne vas apparaitre même pas commentaire (0).

Si un utilisateur ne renseigne pas son nom et laisse un string vide, on renverra egalement rien car c'est un false.

## Inverser un falsy

Si je veux que l'utilisateur, quand il ne met rien dans le string, la valeur de renvoi soit quelque chose d'autre,
j'utilise le ! devant le nom de la variable.



