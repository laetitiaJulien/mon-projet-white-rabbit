#React 

framework qui permet de créer des interfaces utilisateurs intéractives.
Il se compose de composants autonomes qui peuvent être assemblé pout créer des interfaces utilisateurs complexe.
Lorsque les données changeront React mettra à jours que les composants qui en auront besoins. 
 
**A retenir** 

- React est une bibliothèque JavaScript 
- React n'est pas un cadre (different par rapport à Angular)
- React est un projet open source crée par Facebook 
- React est utilisé pour construire des interfaces utilisateurs 
- React est la couche de vue d'une application MVC (model view controller)


## Create React App 

C'est un environnement plus confort pour apprendre React et la meilleur option pour démarrer. 
Il configure l'envirronement de développement de développement pour pouvoir utiliser les dernières fonctions de JS.
Pour créer une nouvelle appli web en single page c'est l'idéal.

**Attention** Creat App ne prend pas en charge la logique coté server ni les bases de données.
Il gère la partie de sorte que l'on peut utiliser les servers que l'on veut. 

## La partie typage 

React fonctionne aussi bien avec les normes de codage JavaScript qu'avec les normes de typage TypeScript. 

### JSX 

Jsx est une extension syntaxique de JavaScript. Elle permet notamment d'insérer de return dans une fonction une DIV 
en mettant des parenthèses.

[JSX](../assets/JSXreturn.PNG)
 
### Bind 

Dans une fonction on définit la fonction hello sans le this.

et bind sert a passer en argument la valeut toto, c'est comme si on lui créait un this.  





