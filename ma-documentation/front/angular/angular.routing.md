# Angular module

## Récap

On a vue que Angular ce composait de module et de component.

Dans un module on peut avoir plusieurs component certain vont  être utile au module et 
d'autre vont être exporté pour servir à d'autre modules.

### le rooting

Le Angular RouterNgModule fournit un service qui vous permet de définir un chemin de navigation entre les différents états de l'application et d'afficher les hiérarchies dans votre application. Il est calqué sur les conventions de navigation habituelles du navigateur:

Le routeur mappe des chemins de type URL vers des vues au lieu de pages. Lorsqu'un utilisateur effectue une action, telle qu'un clic sur un lien, qui chargerait une nouvelle page dans le navigateur, le routeur intercepte le comportement du navigateur et affiche ou masque les hiérarchies de vues.

Si le routeur détermine que l'état actuel de l'application nécessite des fonctionnalités particulières et que le module qui le définit n'a pas été chargé, le routeur peut charger le module à la demande sur demande.

Le routeur interprète une URL de lien en fonction des règles de navigation et de l'état des données de votre application. Vous pouvez naviguer vers de nouvelles vues lorsque l'utilisateur clique sur un bouton ou sélectionne dans une liste déroulante, ou en réponse à un autre stimulus de n'importe quelle source. Le routeur enregistre l'activité dans l'historique du navigateur, de sorte que les boutons Précédent et Suivant fonctionnent également.

Pour définir des règles de navigation, vous associez des chemins de navigation à vos composants. Un chemin utilise une syntaxe de type URL qui intègre vos données de programme, de la même manière que la syntaxe de modèle intègre vos vues avec vos données de programme. Vous pouvez ensuite appliquer la logique du programme pour choisir les vues à afficher ou à masquer, en réponse à la saisie de l'utilisateur et à vos propres règles d'accès.



#### Module pour créer et exporter des components

Pour cela on utilise **déclaration** pour déclarer les components.
si ce n'est pas le cas un message d'erreur s'affichera.

Il est possible qu'on ne veuille pas exporter certain component, dans ces cas là on a le droit de ne pas mettre export 

##### Module pour fabriquer des pages et structurer l'application

Prenons l'exemple d'un site de poneys:

- On va définir 4 pages 
    * Home 
    * About
    * CGU
    * Liste poneys
    * Détails poneys 
    
![shemas poneys module](../../assets/shemasponeysmodule.png)

Pour cela on va avoir besoin de module spéciaux :

- Un main Module
qui va contenir tout le monde 

- Un main routing module ou il n'y aura que des routes(pas de component)


###### Les sous modules

Dans la dernière page (détail poneys) on va trouver des sous onglets qui seront des sous modules 

![shemas sous module](../../assets/shemassousmodule.png)


Et dans ces onglets quand on cliquera dessus on aura des sous pages qui seront des issus de ces sous modules. 

![stand poneys](../../assets/poneystand.png)

Le faite de découper les route permet de ne pas charger l'ensemble des données en un seul appel.
Car si on charge beaucoup de données (d'information sur les poneys) cela prend beaucoup de bande passante et ralenti le 
site. 

C'est pour cela qu'il est trés important de bien faire ses routes.


## Dans la pratique

Pour réaliser ce site on va commencer on va créer 3 modules :

- Home
- About
- Poneys 

(CGU est passé à la trape)

et un dans poney un sous module. Pour faire deux sous module dans la console tapé :

ng g m poneys/poneys-list
             /poneys-detail
             
**La différence entre un module qui sert à faire des pages et un module qui sert à créer des component c'est que dans le 
module de page il n'y a pas de export**  
           
![module de page](../../assets/module%20de%20page.png)
             
Ensuite on créer les components 

*Attention* à bien placer les components dans le bon module


Dans chaque module créer un poneys.routing.module ou poney-list.routing.module ...
et mettre forChild à la place de forRoot.

**forRoot** représente la racine et on ne peut avoir qu'une seul racine(dans app.routing.module )

![poneys routing module](../../assets/poneyroutingmodule.png)

Ensuite dans app.routing.module on va déclarer les routes de l'application.

Pour cela  
- on utilise **path** à la racine 
- puis on va chercher les enfants donc **children**

Dans children on va réutiliser path mais cette fois-ci on va charger **about**
et pour aller chercher le module on utliser **loadChildren** qui est une fonction qui utilise **import**.

Dans import on indique le chemin du module et son nom

![les routes](../../assets/les%20routes.png)

Les seuls modules qui ne vont pas être notifiés sont :

- poneys-detail
- poneys-list

Vue que se sont des sous routes ils doivent être déclarés dans le module concerné
donc poneys

Dans poneys.routing.module il faut déclarer les 2 sous routes :

![sous routes](../../assets/2%20sous%20routes.png)

Maintenant on va renseigner les routes des modules enfants c'est à dire 
about.routing.module qui va commencer par un **path** vide puisque la routes de about est déjà déclaré dans app.routing.module.
Par contre il faut charger le component de about.

![about route](../../assets/aboutroute.png)

Ensuite importer le about.routing.module dans son module c'est à dire about.module
 
 ![aboutmodule](../../assets/aboutmodule.png)
 
Petite particularité pour poney-detail vue que l'on charge l'id la route va être lègérement différente dans
poneys-detail.routing.module 
 
 ![detail poneys](../../assets/idponeysdetail.png)
 
Maintenant que les routes sont créer on veut qu'elles apparaissent sur notre page pour ça
il faut utiliser le **rooter-outlet** dans le app.component.html
 
et faire un menu en forme de list en utilisant les balises *ul* et *li* dans lequel on va insérer des lien appeler 
**routerlink**

![menu poneys](../../assets/menu%20poneys.png)

par contre pour afficher les sous modules même si il n'y a rien à la racine et
pour que ceci s'affiche : monponeysmagique/poneys/poneys-list dans l url

![racine vide](../../assets/racine%20vide.png)

et faire pareil dans poneys-list.

Maintenant on aimerai qu'il s'affiche réellement sur la page 

![poneyslist](../../assets/menu%20poneys%20list.png)

Maintenant on voudrait que sur l'url il n'y ai pas home mais rien on va donc 
enlever le redirected to et home de path

![path modif](../../assets/path%20home.png)


pareil pour poneys-list et poneys-detail

![route poney-list](../../assets/modif%20route%20sous%20component.png)

et faire la modif également sur le menu

![menu sous module](../../assets/menu%20sous%20module.png)

modifier aussi le menu principal en enlevant home 





 
 

 
