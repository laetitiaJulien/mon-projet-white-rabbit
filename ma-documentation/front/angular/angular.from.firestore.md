# Ajouter Angular à Firestore et créer un formulaire

## Etapes  d'install

- La première chose c'est d'ouvrir firestore et d'aller dans **Project Overview**

- Cliquer sur Ajouter une application et choisir web

- Ensuite dans **Enregistrer l'application** écrire front et cliquer sur enregistrer

- Ensuite dans Ajouter le SDK Firebase  copier le suivant :

![firebase config ](../../assets/firebaseconfig.png)

- Ensuite dans webstorm coller ce code dans environnement.ts

![environnement](../../assets/environnement.png)

- Et faire la même chose de environnement.prod.ts

On va aussi télécharger le plugin angularfire 

Pour ça on va taper dans le moteur de recherche angularfire 
et copier ceci :

![angularfire](../../assets/angularfire.png)

- Et dans la console on se met dans front/myangular et colle la ligne précédente

Ensuite on va ce que l'on appelle bootstrapé pour angular s'initie avec firebase

- aller dans le Getting starting

![getting started](../../assets/gettingstarted.png)

et copier cette ligne 

![angularfiremodule](../../assets/angularfiremodule.png)

- Aller dans app.module.ts et copier la ligne  dans les import puis alt entrée pour les importés 
attention sur environnement choisir celui sans prod à la fin

Dans le getting started à la suite de cette ligne (étape 6) il y a pleins de modules notamment le module 
d'authentification

![authentification](../../assets/authentification.png)

**authentification anonyme** permet de garder les données réaliser sur le site même si on n'est pas authentifié

Il y a également **storage** qui permet de uploader des photos ou des vidéos 

et d'autre modules.

Celui qui nous intéresse c'est le database module

![database module](../../assets/database%20module.png)

ensuite on le copie dans app.module.ts dans les import et en l'importe avec alt entrée

## Créer un formulaire

Pour cela on créer un formulaire ou on va injecter des dépendance dans le formulaire en utilisant le mot clés 
private dans le conctuctor dans poneys-detail.component.ts

- Ensuite on appelle le formbuilder

- Et on initialise un formulaire en l'appelant formgroup
et on l'initialise dans ngOninit

- Et on lui passe un objet dans lequel on lui donne les informations à stocker 

- dans la première clé on peut passer comme première objet ce que l'on mais la logique voudrait qu'il soit null

- ensuite le deuxième objet; toujours dans name; on lui mais un tableau de validator (pour déterminer ce qui est obligatoire
ou pas) par exemple validator.minlength(3) veut dire qu'il faut rentré trois caractère minimum

On peut continue avec d'autre clés age color size avec à chaque des objets (validator...) que si c'est nécessaire
par exemple on ne peut pas mettre de caractère minimum à age car c'est pour les string donc ça n'aurai pas de sens

![formulaire](../../assets/formulaire.png)

Maintenant que l'on a initialiser notre formulaire on va le mettre côté vue :

donc dans poneys-detail.component.html on utilise la balise form et enlève action et à la place on va mettre ngsubmit

Par contre ngsubmit attend une fonction donc dans poney-detail.component.ts on va créer un fonction Onsubmit

![onsubmit](../../assets/fonction%20Onsubmit.png)

Ensuite on revient donc sur la vue poneys-detail.component.html et on passe la function Onsubmit à ngsubmit
puis à l'intérieur on créer des **label** et à l'intérieur des label des **Input**

- bien typer si c'est du texte ou des numbers

- et placer un placeholder est mettre le nom que verra l'utilisateur

Pour que le formulaire côté javascript et que le formulaire html communique on doit l'importer

- donc dans poneys-detail.component.html on appelle formgroup et importe le reactive proposer par webstorm
et mettre notre poneyform

Ensuite pour reprendre les clés fait dans javascript en place formControlname et on met le nom de la clé utilisées 
précédemment 

Pour finir le formulaire on va créer deux boutto un pour valider et un pour supprimer

- Avec la balise button on va y mette type"submit" pour envoyer
- et le deuxième avec une balise aussi button et type"button" pour cancel

![formulaire vue](../../assets/formulaire%20vue.png)

Comme ce qui nous intéresse se sont les valeurs du formulaire on va modifier notre fonction
en lui ajoutant .value dans poneys-detail.component.ts 

![formulaire value](../../assets/formulairevalue.png)

Maintenant on voudrais empécher les personnes de valider le formulaire s'il n'est pas correct

Pour cela il faut aller côté vue et dans la balise du boutton sauver on va passer **disabled** et poneysform.invalid
pour lui dire que si poneyform n'est pas valide le button ne s'active pas (il faut que tout les champs soit remplis)

## Importer les données du formulaire dans la base de données
 
Pour cela il faut créer un service donc dans le terminal ng g s service/poneys

C'est lui qui va donc gérer les appels :

- donc pour ça dans son constructor appeler private afs: AngularFirestore.


Ensuite on veut ajouter un nouveau poneys donc toujours dans service on va créer une fonction addnewponeys

![poneys service](../../assets/serviceponeys.png)

Mais notre données et de type poney.model que l'on n'a pas encore définis
du coup on doit créer un poney.model 

- créer un dossier model
- dedans on y met un fichier poney.model.ts
- dans le fichier on ce sert de notre formulaire pour créer un model ce qui donne ceci .

![poney model](../../assets/poneymodel.png)

ensuite on revient dans notre service ou l'on va importer le poneymodel.

ensuite comme avant on appelle la collection() poney

on add() un nouveau poney par contre add et une promise et les promise ne sont pas admise en angular
donc on va devoir le transformé en observable 

- Pour cela on ajoute return from devant la ligne de code 
et cette observable est de type document referrence

![add poneys ](../../assets/observable%20de%20addponeys.png)

Maintenant on va pouvoir appeller le service dans poneys-detail.component.ts 

- avec private poneyService :poneyService

et on va appeler les données récupérer dans le formulaire et les passer dans notre fonction add


![donnees dans fonction](../../assets/formuin%20fonction.png)

maintenant on get des données donc on va créer des fonctions getAllPoneys
c'est un observable donc on va penser à mettre ce signe $ aprés le nom de la fonction 
get est déjà un observable donc pas besoin d'utiliser le clés **from**.

dans cette exemple on utilise **valueChanges** qui permet d'observés les données mais il est important de typé la collection 

![get poneys](../../assets/getponeys.png)

Ensuite on l'initialise dans le fichier poneys-list.component.ts dans le constructor.

Puis on appelle la fonction dans ngOninit.
Et on intercepte les données et on s'abonne à elles (.pipe; .subscribe)

![ngOninit getAllPoneys](../../assets/ngOninit%20getAllponeys.png)

pour que la vue soit en accord avec le reste et que l'on est bien une liste de poneys 

- on va sur le fichier poney-list.component.html et on liste les poneys en reprennent les clés 

![liste des poneys](../../assets/liste%20des%20poneys.png)

Maintenant on aimerai que quand on clique sur un des poneys on puissent accéder à ses détails.

donc on va d'abord aller dans poneys model et ajouter une clé id

![id poneys model](../../assets/id%20poneysmodel.png)

## Côté back

On va ensuite créer un dossier back (onPoneysCreated) avec un fichier index.ts 

- on = déclencher la fonction quand un quelque chose va se produire

Dans ce fichier on a :

- export puis le nom de notre fonction
- .region(region west 1) car par défaut les server sont placé aux Etats-Unis hors si on utilise ceux en Europe ce seras 
plus rapide
- .runWith pour tourner avec 256mb 
- .firestore pour écouter le firestore
- .document ('poneys/poneyUid) pour écouter les changements sur la liste poneys est plus spécifiquement les poneyuid 
et à chaque fois qu'il y aura un changement on va écouter :

- .oncreate (on peut avoir également onUpdate, onDelete et onWrite)
- quand on créé on récupère un snap de la base de donées de l'objet qu'on vient de créer.
- on lui indique que c'est un snap.data de type PoneyModel
**attention** le PoneyModel doit être collé dans src avec comme particularité l'import de Timestamp
**TimeStamp** permet d'enregister des date sous pleins de form différentes

![timeStamp poneymodel](../../assets/timestamp.png)

- Ensuite on va stocker snap.data dans une const newPoney et on va pouvoir le manipuler en lui ajoutant
 les données qu'on a envie 
 
- Comme l'uid du poney et que l'uid correspond à l'id du poney avec snap.id

- Ensuite on va faire ceationDate ou on va utiliser un outil proposer par firestore.
Cette outil va prendre le moment exacte ou la fonction complète va s'exécuter et mettre timestamp.
Donc ça va prendre le moment exact ou la fonction va mettre l'id et pas quand il est créé en base de données

![onPoneyCreated](../../assets/fonction%20%20onPoneyCreated.png)

Maintenant on veut déployer cette fonction

- On va pour cela aller dans package.json et modifier les script.
- dans serve on va coller le nom de la fonction et ajouter --only function  
- et faire la même chose dans deploy 
On fait ceci car cela permet de ne pas deployer toute les fonctions et qui sont dans notre projet.

![deploy function](../../assets/deploy%20fonction.png)

- Ensuite on lance deploy cela peut mettre un peut de temps pour le premier deploiement 

- pour voir le délpoiement on va dans firebase puis dans la console de firebase
- on clique sur le projet en cours puis dans function 
- une fois le déploiement terminé dans la console on rafraichie pour voir le résultat 

![deploy](../../assets/deploimentprojet.png)

**astuce** si on clique dans **statistique d'utilisation détaillée** cela mène a google cloud platform

![googlecloudplatform](../../assets/googlecloudplatform.png)

cela permet de voir le déclencheur, le code source et teste (pour tester à la mano des fonctions)
ainsi que visioneuse de journaux pour avoir des infos  sur la vie de la fonction

![visionneuse de journaux](../../assets/visionneuse%20de%20journaux.png)

Pour tester la fonction on va créer un poney dans le formulaire et vérifier qu'il apparait bien en base de données 

## Retour au front 

Maintenant on va supprimer la collection poney dans database 

![supprimer collection](../../assets/supprimer%20la%20collection.png)

Ensuite on peut créer de nouveau poneys dans la database 

Maintenant pour que la vue soit actualiser le PoneyModel créer en back on va le copier et le coller dans le PoneyModel
de l'application en front par contre on va devoit importer timestamp

![timestampfront](../../assets/importer%20timestamp%20front.png)

puis il va nous demander d'importer firebase et dans l'import penser à mettre /app

![import firebase/app](../../assets/import firebase:app.png)

Ensuite dans la vue ajouter poney.uid

![poneyvueuid](../../assets/poneyuid.png)

Une fois que le lien est créer on va récupérer l'uid pour en faire quelque chose:

- donc on va dans poney-detail.compenent.ts et on va lire l'url en utilisant **private activatedroute**

- Ensuite on va dire a angular tu vas écouter la route pour voir quel est le paramètre ajouté.
donc on va initialisé ça dans ngOnInit.
On lui demande pour l'instant de simplement console.log le params mais de transformer l'id pour que la clés n'apparaissent pas 

**le map ici est différent de celui qu'on utilise pour les arrays**
ce map écoute toute les data envoyée et chaque fois qu'un data arrive tu la prend et tu la transforme

![params id](../../assets/paramsidfront.png)

Maintenant que l'on a l'id on aller chercher ses data 

donc on va dans le poneys service en créant un observable 

![getponeybyid](../../assets/getponeybyid.png)

Maintenant on aimerai transformé le flux pour pouvoir s'abonner à l'objet poney 
je te donne une id et tu me donne un poney 

- pour cela on utilise **switchMap**  switch pour change et map pour transforme

![recup poney](../../assets/récupération%20du%20poney.png)

Maintenant que l'on à récupérer le poney on voudrait que sur la vue les champs du formulaire se remplisssent automatiquement

- pour cela on créer un poney dans le fichier poney-detail.component.ts 
et on modifie le ngOninit de cette manière

![poneyrecupform](../../assets/poneyrecupform.png)

Par contre on veut garder les champs vide quand c'est un nouveau poney donc 
on va modifier le formulaire en ajoutant **poney ?**
pour dire si le poney est la les champs sont prêt rempli et sinon ils sont vides

![initform](../../assets/initformulaire.png)

puis on initialise le formulaire

![initialiseform](../../assets/initialiseform.png)

par contre on ne veut pas que l'on puisse ajouter un nouveau poney si il est déjà existant pour cela on va mettre des conditions
) **onSubmit**  

![onsubmit modif ](../../assets/onsubmit.png)

ensuite dans service pour créer une fonction updatePoneyByUid

![updatePoneyByUid](../../assets/updatebyponeyuid.png)

ensuite on re modifie onsubmit

![poneyform onsubmit](../../assets/onsubmitponeyandponeyform.png)

pour créer un poney et que ce soit plus clair on modifie le nom de la route 

![creer un poney](../../assets/créerunponey.png)

pour ne pq enregistrer encore un nouveau poney et faire un reset le formulaire

![reset formulaire](../../assets/resetform.png)

 



 



 



 














 


   

 
