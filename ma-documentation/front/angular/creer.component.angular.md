# Créer un component 
Les component définissent des domaines de responsabilité dans l'interface utilisateur, qui vous permettent de réutiliser des ensembles de fonctionnalités d'interface utilisateur. 

Un component se compose de trois choses:

**Une classe de component** qui gère les données et les fonctionnalités. Dans la section précédente, les données de produit et la méthode de la classe de component gèrent respectivement les données et les fonctionnalités.

**Un modèle HTML** qui détermine l'interface utilisateur. 

**Styles spécifiques** aux component qui définissent l'apparence.C'est là que réside le composnent CSS.
Une application angular comprend une arborescence de component, dans laquelle chaque component angular a un objectif et une responsabilité spécifiques.



Pour rappelle un component permet de mettre des éléments redondant dans un site.

## Créer un module

Dans le dossier my angular créer un module au nom de avatar (toujours créer le module avant le component)

commande ng g m

![command ng g m](../../assets/ng%20g%20m.png)



**Attention** un component ne ce déclare que dans un seul module  

## Créer le component

dans la console et écrit ceci :

cela va permettre de créer un component Avatar composé de plusieurs fichiers

![commande ng g c](../../assets/ng%20g%20c.png)

## Exporter un component
  
Dans le module on déclare les components (cela est fait automatiquement normalement),
ensuite on met le mot-clé **export** pour exporter les components liés au module (sachant qu'il peut en avoit plusieurs)

![export component](../../assets/exportcomponent.png) 

## Rendre disponible un component 

Pour pouvoir utiliser app.component dans app.component.html
on doit le déclarer dans app.module

![app.component.html](../../assets/app.component.png)

![app.mdule](../../assets/app.module.png)

## Créer un image par défaut

- Prendre une image sur internet

- la stocker dans le dossier assets

- Ensuite on la place dans app.component.html dans une balise img

![avatar img](../../assets/avatarimg.png)

OU 

- On peut copier l'URL d'une image sur internet 

- Et la placer sur dans une balise img toujour dans le même fichier

![imqge url](../../assets/img%20url.png)

** attention au CROSS ORIGIN qui vous donne ou non le libre accés aux images.

## Créer une image par défaut si la personne n'en a pas 

Pour que le component parent( app.component) puisse communiquer avec le component enfant(avatar.component), 
on va placer le mot clés **Input**.

- dans ce Input on va mettre notre avatar url local par défaut

 ![input avatar url](../../assets/inputavatar.png)
 
 
- et dans la balise img de avatar.component.html  on va passe avatar url. 

 ![img avatar.component](../../assets/img%20avatarurl.png)
 
 Ceci nous donnera par défaut l'image ce trouvant dans nos dossiers
 
## Mettre une image déja existante de la personne par son URl
 
 On voudrait que comme une fonction l'URL de l'image passe en argument
 
 - On stocke l'URL dans le component principal app.component et on nomme la fonction avatarUrlFromMain
 
 ![avatar URL app.component](../../assets/avatarfrommain.png)
 
- Est dans app.component.html on avatarUrlFromMain

![avatarUrlFromMain](../../assets/avatarurlfrommain%20app.component.png)

## Mettre une note a une personne 

Dans avatar.component.ts mettre un nouvel *Input*
par défaut on la met a zéro

![input note](../../assets/input%20note.png)

- Ensuite je l'appelle dans avatar.component.html dans une div

![input rating](../../assets/input%20rating.png)

quand on met un élément entre [] on va interprêter les données.
dans les double quotes si on met un string il le prendra sous forme de number
pour modifier cela il faut penser à ajouter des simple quote dans les double quotes

## Informer le parent d'une modification chez l'enfant 

On va créer un boutton *vote* dans avatar.component.html

![button vote](../../assets/buttom%20vote.png)

- Dans le père app.component.ts on va lui ajouter une variable vote qui vos 0

![variable vote](../../assets/variable%20vote.png)

et toujours dans le père mais dans app.component.html on va appeler la variable

![vote](../../assets/vote.png)

Maintenant on aimerai que à chaque fois que l'on appuie sur le boutton vote, le parents soit au courant 
et que se soit comptabilisé dans la console

- Pour cela on créé une fonction vote dans avatar.component.ts

![function vote](../../assets/function%20vote.png)

- Ensuite sur le button on fait un appel à la fonction vote

![button function vote](../../assets/button%20function%20vote.png)



Maintenant on voudrais que ça s'affiche plus seulement dans la console mais aussi sur la page

- Faire une variable voteNumber = 0 dans le fils avatar.component.ts

- Et toujours dans le même fichier au niveau de la function appeler voteNumber

![vote number](../../assets/vote%20number.png)

- Dans le fichier app.component.html on passe la function

![voteNumber dans html](../../assets/votenumbers%20h.png)

Ensuite on voudrais que non seulement cela s'affiche sur la page mais également que le père soit au courant 

Pour cela on va placer un *Output* pour exporter les données et également un Event Emitter pour qu'il soit au courant des 
changement dans avatar.component.ts

- Cette emission on la nomme *isvotedone*

![event emitter](../../assets/eventemitter%20output.png)

- On va créer une fonction qui permette d'ecouter chez le père

![change vote](../../assets/function%20change%20vote.png)

- On voudrait maintenant que cette emission soit écouté 
donc on va dans app.component.html

![ecoute changevalue](../../assets/ecoute%20changevalues.png)

Penser a modifier vote et a ajouter event emitter

![event emitter vote](../../assets/eventemitter.png)

Pour reprendre l'eventemitter on va l'appeler comme ceci $event 

![$event](../../assets/$event.png)






 




 
 
