# Angular Service

## Comment lier Angular avec node

Node est une API de CRUD (lire, écrire, modifier des choses) et à des applications externe de leurs parler.
Donc on va utiliser Angular pour lui poser des questions. 

 **attention** ne pas faire les appels des fonctions dans le component de Typescript. Il faut pour cela utiliser le service
 
 
 ### Le service
 
Pour les données ou la logique qui ne sont pas associées à une vue spécifique et que vous souhaitez partager entre les composants, vous créez une classe de service . Une définition de classe de service est immédiatement précédée par le décorateur. Le décorateur fournit les métadonnées qui permettent à d'autres fournisseurs d'être injectés en tant que dépendances dans votre classe.@Injectable()

L'injection de dépendance (DI) vous permet de conserver vos classes de composants lean et efficaces. Ils ne récupèrent pas les données du serveur, ne valident pas les entrées utilisateur ou ne se connectent pas directement à la console; ils délèguent ces tâches aux services.

Comme les compenents et les modules c'est une classe qui à la particularité d'utiliser le singleton.
 
**singleton** est une classe qui a qu'une seule instance et qui n'éxiste que dans le cadre de leur module  
principe universel de developpement (livre design patterns)

#### Créer un service 

- Aller dans le terminal et taper ceci (hotels car il nous aide a trouver des hotels)

![ng service](../../assets/ngservice.PNG)

- Effacer le fichier spec.ts généré car il on ne va s'en servir, pas dans le cas présent 
et giter l'autre


## Faire des appels au backoffice

Pour faire les appels au backOffice on va avoir besoin de faire des appels http. 
En Angular pour se servir d'un outil il faut le déclarer dans le constructor. 
Mais pour faire les appels on a également besoin d'appeler les HttpClientModule dans le app.module 

![HttpClient](../../assets/HttpClientModule.PNG)

Pour ce servir de cet outil, il faut le notifier dans le constructor 
mettre le mot clé Private pour signifier qu'on l'utilise que dans le constructor. 
et donner un nom à l'appel ( pour ne pas s'embêter à donner un nom shift + f6 et l'auto-complétion se fait automatiquement)

![nom HttpClient](../../assets/nomhttpClient.PNG)


Ensuite on veut faire un GET avec donc une méthode (ou function mais qui est dans une class) on a plus besoin 
du mot function. 
Et dans get on met notre url et le get renvoi un observable 

![get de service](../../assets/getservice.PNG)


**observable** comme les promises dans node mais en plus complexe à manipuler. On peut comparer ça un grand tuyau où l'on
passe des datas, on va écouter les datas et où on s'abonner aux datas (subscribe)

dans un observable on utilise pas de .then mais pipe. 

**pipe** sert à traiter la data obtenue. 

![pipe](../../assets/pipe.PNG)

Pour préciser ce que va nous renvoyer Get comme type de data on peut le mettre entre <> 

![typeget](../../assets/typeget.PNG)

Ensuite dans le pipe on peut utiliser des opérateurs de manipulation. 
(mettre des tuyaux annexe)

- Premier opérateur tap pour faire le console.log de hostels

![tap](../../assets/tap.PNG)


**subscribe** ne se fait pas dans le service 
 


 





 
