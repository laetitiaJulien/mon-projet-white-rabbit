# Behaviour subject 

Comment faire pour synchroniser des composant entre eux?

Exemple on veut que le nom de l'utilisateur apparaissent aussi bien sur le header, sur le footer et sur l'une des pages.

On peut utiliser les @input mais quand un component est trop haut dans la hiérarchie ou trop bas il est difficile de les 
fair communiquer entre eux. Des input dans des inputs qui donc quand on voudrait en changer un il faudrait changer tous
les autres.

## 

créer le footer et le header (ng g module et ng g component...)

- on export le footer component dans footer module pareil pour le header

Ensuite dans la vue de notre application app.component.html on tape app-header et on importe le module

pareil pour le footer

![app-header](../../assets/app-header%20et%20app-footer.png)

Ensuite on va simuler qu'il y a un nom d'utilisateur.

- dans database créer une nouvelle collection appeler users

![new collection](../../assets/newcollection.png)

- et on va remplir les champs demander

![collection users](../../assets/collectionusers.png)

Une fois notre users créer on pourrait pour qu'il s'affiche sur notre site dans le header et dans le footer faire deux 
appels à la base de données mais se serait inutile puisqu'on peut le faire qu'une seul fois et le partager aux autres 
component 

On va aussi mettre un début de titre dans la page ou le no, de l'users apparaitra également 

![titre avec users](../../assets/titreusers.png)

Donc pour appeler les users on va créer un users service (ng g s service/user)
on pense à importer angular firestore

![service angularfirestore](../../assets/serviceangularfirestore.png)

On créer sa fonction getUserById 

![getusersbyid](../../assets/getusersbyid.png)

On va créer un UserModel

![user model](../../assets/usersModel.png)

Ensuite dans le service on va stocker lastName puis pipé les données

![users service](../../assets/usersservice.png)

Ensuite dans le app.component.ts on implement ngOnInit et on appelle Userservice

![init user](../../assets/init%20users.png)

on écrit la même chose dans le header.component et dans le footer component
c'est à dire 

![footer init](../../assets/footerinit.png)

puis on va l'appeler dans la vue du footer et du header et dans le app.component.html

![vue header](../../assets/vueuserheader.png)

Maintenant on veut que tout les components qui appeller lastName attend d'executer la fonction quand l'info est prête 
et ça c'est le service qui va le gérer.

Pour faire cela on créer un observable et plus particulièrement un sujet d'observable 

- Donc dans service on va créer lastName$ (observable): **behavioursubject**

**behavioursubject** aui veut dire sujet comportemental pour étudier un comportement
comme notre users va changer il faut bien l'étudier 

![behaviour subject](../../assets/behaviour%20subject.png)

Ensuite pour le remplir des valeurs quand elles arrivent

- on va placer un nouveau .tap et dans celui-ci on ajoute .next pour lui dire que quand lastName$ (observable) change; tu dois également 
changer lastName (pas observable)

![next users](../../assets/nextusers.png)

Dans le header on modifie ngOnInit pour s'abonner à l'observable 

![ngoninit observable users](../../assets/ngoninit%20observable%20users.png)

copier coller dans le footer.

Par contre dans app.component.ts vue qu'il subscribe déjà getUsersById on ne peut pas juste copier coller le code et avoir 
deux fois subscribe dans un seul ngOnInit 
par contre on peut les rassembler en créant deux fonctions 

- on va créer une première fonction getUser avec ce qu'il y avait précédement sans le subscribe
- et créer une fonction getlastName sans le subscribe 

ajouter return à chaque fois 


![getUser et getLastName](../../assets/getuseretgetlastName.png)

maintenant on utiliser un outil pour subscriber les deux à la fois qui s'appelle **combinelatest**
qui prend en paramètre un tableau d'observable

![combinelatest](../../assets/combinelatest.png)

on peut également reprendre les données directement dans le combineLatest au lieu de faire des console.log dans 
dans les fonction getuser et getLastName

![combinelatest pipe](../../assets/combinelatestpipe.png)


on peut par contre subscriber plusieurs fois si ce sont des promesses car la promesses trigger qu'une seul fois 
même les promesses transformer en observable par from 

 











  

 