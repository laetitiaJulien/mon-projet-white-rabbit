# Angular

## Qu'est ce que Angular 

Angular permet de repéter sur plusieurs page d'un site internet 
les mêmes éléments (ex: menu, pub, header...) grâce à des components et des modules 

Angular est une plate-forme et un framework pour construire des applications clientes en HTML et TypeScript. Angular est écrit en TypeScript. Il implémente les fonctionnalités de base et facultatives sous la forme d'un ensemble de bibliothèques TypeScript que vous importez dans vos applications.


![shémas site internet](../../assets/shémassite.PNG)

### Qu'est ce qu'un module 

Regroupement de contenus comportant le même thème. 
Les blocs de construction de base d'une application angular sont NgModules , qui fournissent un contexte de compilation pour les components. NgModules collecte le code associé dans des ensembles fonctionnels; une application angulaire est définie par un ensemble de NgModules. Une application a toujours au moins un root module qui permet le démarrage, et a généralement beaucoup plus de modules de fonctionnalités .

Chaque application Angular possède un module racine ou root module , conventionnellement nommé AppModule, qui fournit le mécanisme d'amorçage qui lance l'application.

Comme les modules JavaScript, NgModules peut importer des fonctionnalités à partir d'autres NgModules et permettre à leurs propres fonctionnalités d'être exportées et utilisées par d'autres NgModules. Par exemple, pour utiliser le service de routeur dans votre application, vous importez le RouterNgModule.

L'organisation de votre code en modules fonctionnels distincts aide à gérer le développement d'applications complexes et à concevoir en vue de sa réutilisabilité. De plus, cette technique vous permet de tirer parti du chargement différé, c'est -à- dire du chargement de modules à la demande, pour minimiser la quantité de code à charger au démarrage.

ex module publicité 

![module pub](../../assets/module.PNG)

### Qu'est ce qu'un component 

Les components définissent des vues , qui sont des ensembles d'éléments d'écran qu'Angular peut choisir et modifier en fonction de la logique et des données de votre programme.

Les components utilisent des services qui fournissent des fonctionnalités spécifiques qui ne sont pas directement liées aux vues. Les fournisseurs de services peuvent être injectés dans les components en tant que dépendances , ce qui rend votre code modulaire, réutilisable et efficace.

le component compose le module (sous module)

si on reprend l'exemple de la publicité, on aurai une publicité en long et un publicité sur le côté 

![compenant](../../assets/compenant.PNG)

l'avantage du component c'est qu'il va s'auto-initialiser tout seul, et peut se placer n'importe où. 

les component on, pour pouvoir fonctionner, la possibilitée de passer des entrées et des sorties.
les component peuvent se réutiliser.

**exemple** dans le component pub il y a donc plusieurs component (1.2.3) et le component père peut leurs transmettre des 
infos ( ex rafraichie toi toute les 10 minutes pour changer la pub).

**Attention** il est interdit de copier coller quelque choses de récurrent que ce soit dans du Html ou dans du Typescript 
quelque chose qui n'est pas dans un component 

## Instalation de CLI

### Qu'est ce que CLI 

Command line interface, 
Une interface en ligne de commande (couramment abrégé CLI) 
est une interface homme-machine dans laquelle la communication entre l'utilisateur et l'ordinateur s'effectue en mode texte :

l'utilisateur tape une ligne de commande, 

c'est-à-dire du texte au clavier pour demander à l'ordinateur d'effectuer une opération,

l'ordinateur affiche du texte correspondant au résultat de l'exécution des commandes tapées ou 
à des questions qu'un logiciel pose à l'utilisateur.
 
 - Sur internet chercher angular cli et copier cette ligne de commande dans le terminal
 
 ![cli install](../../assets/angularCLI.PNG)
 
 ![cli terminal](../../assets/CLITerminal.PNG)

- Aprés l'installation répondre oui à la question 'how to change this setting...' 

- Créer un dossier front dans tryHard 

- ensuite on reprend le dossier front dans le terminal **trés important**

![dossier front](../../assets/frontdirectory.PNG)

- ensuite taper ng --version pour tester et voir si angular c'est bien installé 

- Si pas d'erreur à ce niveau :) youpi on passe à la suite en créant un projet et en lui donnant un nom 

![new projet](../../assets/ngprojet.PNG)

- A la question voulez-vous mettre Angular routing répondre oui 

- Choisir le format SCSS pour le type de format CSS

![format SCSS](../../assets/formatCSS.PNG)

- Aller dans package.json si les dépendencies sont soulignés en orange penser à actualiser le dossier du projet en cours 
en faisant *synchronise* pour prendre en compte les nouveaux dossiers installer par la ligne de commande 

- Ensuite faire clique droit sur package.json et show npm script 
puis start pour tenir l'angular 

- cliquer sur l'url fourni dans le terminal 

![pack bienvenue](../../assets/packbienvenue.PNG)

### Composition de l'app

- Dans les fichiers installés on à l'app qui est divisé en quatre

1. d'abord les fichers racine qui sont nécessaire à son lancement 
avec dedans un fichier html(interdiction formelle d'y toucher )

![fichier racine](../../assets/fichierracine.PNG)

- dans ce fichier html on a de nouvelles balises propre à angular comme app-root. 
Avec Angular on peut également créer c'est propre balises qui serviront pour les component.

![fichier racine html](../../assets/htmlangular.PNG)


- Main.ts est également un fichier d'init à **ne pas toucher**
avec dedans  **platformBrowserDynamic()**(chercher dans l'angular le gestionnaire du browser)

![platformBrowserDynamic](../../assets/plateformBrowser.PNG)

et AppModule fourni par angular qui est obligatoire et qui permet de lancer l'application

2. Le fichier environnement  permet de stocker des données que l'on veut garder
(ex le token de firebase)

3. Le fichier assests pour les images et les polices de caractères

4. Ensuite on  a App dont 

- app.module qui utilise le fameux système de module. 

Pour définir un module on utilise l'@ ex @Mymodule() et dans ce fichier on utilise également le système des classes
avec AppModule.


- app.compenant avec également l'@ pour le définir et le système de classe mais cette fois-ci avec AppCompenent 
Pareil avec le routingModule 

####Modèles, directives et data binding

Un modèle combine du HTML avec du balisage angulaire qui peut modifier les éléments HTML avant qu'ils ne soient affichés. Les directives de modèle fournissent la logique du programme et le data binding connecte vos données d'application et le DOM. Il existe deux types de liaison de données:

La liaison d'événements ou **Event binding**  permet à votre application de répondre aux entrées des utilisateurs dans l'environnement cible en mettant à jour les données de votre application.
La liaison de propriété ou **Property binding** vous permet d'interpoler des valeurs calculées à partir de vos données d'application dans le code HTML.


Avant qu'une vue ne s'affiche, Angular évalue les directives et résout la binding syntaxe dans le modèle pour modifier les éléments HTML et le DOM, selon les données et la logique de votre programme. Angular prend en charge le double binding, ce qui signifie que les changements dans le DOM, tels que les choix des utilisateurs, sont également reflétés dans les données de votre programme.

Vos modèles peuvent utiliser des canaux pour améliorer l'expérience utilisateur en transformant les valeurs à afficher. Par exemple, utilisez des canaux pour afficher les dates et les valeurs monétaires appropriées aux paramètres régionaux d'un utilisateur. Angular fournit des tuyaux prédéfinis pour les transformations courantes et vous pouvez également définir vos propres tuyaux ou pipes.


### Vocabulaire

@MyModule, @MyCompenant et @MyRoutingModule sont **des décorateurs**  ils servent à injecter automatiquement des infos dans une classe en spécifiant le types d'infos que l'ont injecte. 

**Un component** est composé:
- d'un template html (ce qu'on va voir)
- d'une feuille de style (comment va être stylé le projet)
- du javascript qui va permettre de faire quelque chose 


**Le simple binding** entre le component et la vue. Quand je modfie mon contrôleur c'est à dire (app.component.ts) il devient 
disponible dans mon template. 

**Le double binding** on modifie le vue, du coup le component et le template


