# Passage par références

## Qu'est que la mémoire vive ?

La mémoire vive permet à l'ordinateur de stocker les données de manière temporaire.
La différence entre le disque dur et la mémoire vive c'est la vitesse.
Au démarrage de l'ordinateur la mémoire vive est directement accessible au CPU (central processing unit)
Par contre quand on éteins l'ordinateur elle s'éfface.

Dans les languages de programmation notamment JS, lorsqu'on créer un const ou un let les données sont stocker dans cette 
mémoire vive.

Et pour les retouver la mémoire vive les places dans des emplacement possèdant une adresse (du style kjljh....).

Seulement en JS les primitives (String, Number, Boolean, Null, undefined, Symbol) en placées par copie alors que les objets 
eux sont placés directement dans une case de mémoire.

## Mpdifier une primitive

Pour modifier une primitive il va falloir en faire une copie car comme on la vue précédemment 
une primitive n'a pas d'emplacement dédié.
Par la valeur parent n'est pas modifiée.

**exemple**

On veut modifier un titre *My Angular*.

On va crée une autre titre que l'on va appeler *title2*
dans le component

![title2](../assets/title%202.png)

puis on va lui attribuer la valeur *toto*

![title toto](../assets/titile2toto.png)

cela nous donne :

![resultat](../assets/title2totoresult.png)

## Modifier un objet

Par contre pour modifier un objet on est obliger de pointer l'endroit où l'objet est stocker dans la mémoire vive
La copie faite l'enfant va pouvoir modifier également et vice versa.
Alors que ce n'est pas le cas pour une valeur primtive.

Maintenant on va modifier un objet donc:
 
- on créer un objet que l'on nomme *page info*
on met des clés dedans un *title* et une *date*

- puis on en créer un deuxième pour modifier le premier que l'on appelle
*page info2*

![page info2](../assets/pageinfo2.png)

- donc quand on modifie l'original et non la copie cela modifie également la copie 

![resultat page infos2](../assets/resultpageinfos2.png)



