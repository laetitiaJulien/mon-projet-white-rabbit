# Documentation Architecture HTML/ Bootstrap


## Structurer un site

 . Combien de pages vais-je faire pour mon site ? 

Par exemple faire les plus courantes: HOME FAQ ABOUT
 
 . Penser à créer des dossiers communs pour tout les projets (ex : assets, pour les images, 
about pour les liens extérieur)

 . Balises de référencements :

```

<sections> 

<hadder> </hadder>
<footer> </footer>

</section> 

```

La balise section permet de diviser la page en trois sections (ex: article, image, contact)
Dans la balise section on peut mettre une balise Header et une balise Footer. 

Ces balises aide le navigateur à comprendre comment est composé le site.

 . La balise h1 est importante pour faire un bon référencement.

**ATTENTION** : il n'y a qu'un seul h1 par page 

 . Le header peut contenir le h2 

 . Puis en h3 on peut mettre les infos principales

 . Finaliser par le style du site 


Si parmis ces infos, il y en a plusieurs et que l'ont veut les mettre côte à côte on peut utiliser
Bootstrap qui est plus rapide et facile à utiliser

## Qu'est ce que Bootstrap ?

> Bootstrap est un framework de CSS qui donne clés en main des blocks de code,
> Cela permet de gagner du temps lors du codage. 


## Comment utiliser Bootstrape ?

 . Ce référer à la documentation

 . Copier le script ou les scripts dans la page HTML dans la balise <head>

 . Pour utiliser les différentes fonctionnalités se fier à la doc.

[lien documentation Bootstrape](https://getbootstrap.com/)

 . Créer une grille pour gérer la structure du site et gérer le responsive.

 . Pour cela il faut créer une DIV container. (Obligatoire dans bootstrape)

*IMPORTANT* : On ne peut mettre qu'une seule balise container. 

 . Utiliser les DIV row et col pour créer les lignes et les colones 

```

 <div class "row"> 
    <div class "col-4">

</div>
</div> 




```


la div *col* : permet de créer une colone 

la div *row* : permet de créer un ligne 

c'est div vont de paire on ne peut pas avoir de "div" row sans col est vice et versa 

## Infos supplémentaires 

*Dans Webstorm* 

les src sont surligné par Webstorm c'est par ce qu'il faut télécharger la librairie sur l'ordinateur

Pour télécharger la librairie sur l'ordinateur alt + entrée et download librairie 

*IMPORTANT* : Permet a Webstorm de faire l'auto complétion pour enrichir la librairie

*Astuce* : pour créer une DIV 

div.class + Entrée

*Vocabulaire* 

>Un navigateur : est un *parseur* qui permet de retranscrire les infos quelque soit le format 

