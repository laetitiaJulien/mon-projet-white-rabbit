# Comment personnaliser bootstrap?

Les élements composant Bootstrap peuvent être modofié. Cela permet de créer un fichier personnalisé 
Pour personnaliser bootstrap, il faut ce servir d'une feuille de style CSS.

## Les rêgles pour créer une feuille de style CSS :

### Pensez à bien organiser ses dossiers et ses fichiers

Par exemple créer un new directory "style" avec à l'intérieur les feuilles de style personnalisées Bootstrap

### Inclure une feuile de style dans un fichier Html :

Pour ajouter une feuille de style dans un fichier HTML il suffit de créer une balise link avec le fichier en lien 
exemple 

``` 
<link rel="stylesheet" href="../CSS/allstyle/allStyle.css">
```

dans ce lien il y a le premier dossier et tout les sous dossiers qui mène à la feuille de style.

Ce lien doit être placé dans l'en-tête de la page (balise = head)

***ATTENTION*** 
La place du lien dans la feuille à son importance dans la mesure ou le dernier lien copié dans la page l'emporte sur les autres. 

### Créer une modification d'un élément Bootstrape

Pour créer une modification, il faut reprendre l'élément 'la class ou le nom'
dans la nouvelle feuille de style et remettre les paramêtres choisie à la place :
Exemple je veux modifier la couleur primary de Bootstrape
```
.btn-primary {
backgroud-color : red;
color : yellow;
}
```

L'élément btn primary à été modifier et au lieu qu'il soit bleue il sera maintenant 
rouge et jaune 

***ATTENTION***

Si il a un hover il faut reprendre la classe en ajoutant le hover
exemple 

````
.btn-primary:hover {
background-color: red
color : yellow

````

Dans inspecter console on peut accéder à la couleur choisi en cliquant sur la case hover
 
>*Theming* 
>
>Le theming est un autre moyen de personnaliser Bootstrape en créant un ensemble
de couleur; police... sans passer par CSS mais par Sass 

### Organiser ses feuilles de style 

Pour organiser ses feuilles de style, il est important de créer des dossiers et des sous dossiers 
mais pour éviter de trop surchargé le fichier HTML on peut se servir de la balise import

````
@import 'feuille-de-style'
````

le faite d'importer les fichiers sur un notre fichier global enlève le caractère unique de l'élément.
Si il faut modifier un élément mais seulement sur un seul page il vaut mieux créer une feuille de style dédié 


 

