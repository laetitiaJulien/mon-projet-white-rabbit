# Laetitia Julien
## Coordonnées 
33 rue du colonel fabien

95390 SAINT PRIX

0620510359

<Laetitiajulien91@gmail.com>

![Photo CV](../ma-documentation/photo%20cv%202.jpg) 


## *Presentation*

*28 ans, je souhaite devenir Developpeuse Web*

> ## ***Experiences***

> *Janv 2019*

>> **Hotilner, BNP PARIBAS**

> *Mars 2018 Déc 2018*
 
>> **Téléconseillère, GMF**

> *Nov- 2017 Mars 2018*

>> **Téléconseillère, BRED (Banque populaire)**

> *Sept- Nov 2016-2017*

>> **Téléconseillère, BNP PARIBAS PF**

> *Mai-mars 2015-2016*

>> **Conseillère de vente, Magasin Zodio,**

> *Oct-février 2014-2015*

>> **Employée de caisse, Magasin Babou**

> *Juin-Août 2013*

>> **Commerciale stagiaire, Société Gourmets Primeurs**

> *2011-2012*

>> **Assistante de gestion (Formation en alternance) Clinique Clinalliance**

> *2009-2011*

>> **Vendeuse (Formation en alternance), Magasin Francis Batt**

> ## ***Formation***
>> * **L1 psychologie Université de Reims**
>> * **AMF** Certification 
>> * **BTS** Ngociation 
>> * **Bac Pro** commerce 

> ## ***Competences***

>> * Suivie des objectifs commerciaux
>> * Créer des animations pour dynamiser l’équipe commerciale
>> * Suivie d’une entrée en relation client Ouverture de compte à vue, épargne.
>> * Orienter, conseiller et proposer aux clients les solutions les plus adaptées à leur situation financière
>> * Prospection commerciale


> ## ***Hobbies***
>> - Voyage
>> - Netflix
>  - musique