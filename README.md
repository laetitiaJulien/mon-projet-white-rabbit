# mon projet white rabbit

# Sommaire 

## Intégration 

### Bootstrap

- [bootstrap](../ma-documentation/integration/bootstrap/bootstrap.md)
- [perso.bootsrap](../ma-documentation/integration/bootstrap/perso.bootstrap.md)

## Back

- [await async](../ma-documentation/back/awaitasync.md)
- [La boucle for](../ma-documentation/back/boucleFor.md)
- [Les classes](../ma-documentation/back/class.md)
- [Les falsy](../ma-documentation/back/falsy.md)
- [Les fonctions](../ma-documentation/back/function.md)
- [Les objects et les clés](../ma-documentation/back/object.value.object.keys.md)


### Les Arrays

- [array](../ma-documentation/back/arrays/arrays.md)
- [reduce](../ma-documentation/back/arrays/reduce.md)

### firebase

- [firebase functions](../ma-documentation/back/firebase/firebase.functions.md)
- [firebase installation](../ma-documentation/back/firebase/install.firestore.md)

### Node

- [Node](../ma-documentation/back/node/Node.md)
- [Node CRUD](../ma-documentation/back/node/node.crud.md)
- [Patch et gestion d'erreurs](../ma-documentation/back/node/PATCH.gestion%20d'erreur.md)

### Les promesses

- [les promesses](../ma-documentation/back/promise/newpromise.md)
- [Utilisation de promise.all](../ma-documentation/back/promise/promiseall.md)

## Front

- [Passage par références](../ma-documentation/front/passage.reference.md)


### Angular 

- [Angular](front/angular/angular.md)
- [Angular Service](front/angular/angular.service.md)
- [créer un component](../ma-documentation/front/angular/creer.component.angular.md)
- [Les routes d'angular](front/angular/angular.routing.md)
- [Ajouter firestore à Angular](front/angular/angular.from.firestore.md)


## Outils de méthodologie

- [Faire une doc en Markdown](../ma-documentation/outils.methodologie/Documentation.md)
- [Hébergement](../ma-documentation/outils.methodologie/Hebergement.md)
- [Le Merge Request](../ma-documentation/outils.methodologie/Merge%20request.md)
- [Les bonnes pratiques](../ma-documentation/outils.methodologie/les%20bonnes%20pratiques.md)